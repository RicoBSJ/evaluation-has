package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.PersonCriteriaDto;
import com.aubrun.eric.evaluation_Has.business.mapper.CotationDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.PersonCriteriaDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.consumer.PersonCriteriaRepository;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.PersonCriteria;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonCriteriaService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final PersonCriteriaRepository personCriteriaRepository;

    public PersonCriteriaService(EvaluationHasUserRepository evaluationHasUserRepository, PersonCriteriaRepository personCriteriaRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.personCriteriaRepository = personCriteriaRepository;
    }

    public List<PersonCriteriaDto> findAll(){
        return personCriteriaRepository.findAll().stream().map(PersonCriteriaDtoMapper::toDto).collect(Collectors.toList());
    }

    public PersonCriteriaDto findById(int personCriteriaId){
        return PersonCriteriaDtoMapper.toDto(personCriteriaRepository.findById(personCriteriaId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, PersonCriteriaDto personCriteriaDto){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        PersonCriteria personCriteria = new PersonCriteria();
        personCriteria.setPersonCriteriaName(personCriteriaDto.getPersonCriteriaName());
        personCriteria.setCotationSet(personCriteriaDto.getCotationDtoSet().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toSet()));
        personCriteriaRepository.save(personCriteria);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(PersonCriteriaDto personCriteriaDto, int personCriteriaDtoId){
        PersonCriteria personCriteria = personCriteriaRepository.findById(personCriteriaDtoId).orElseThrow(RuntimeException::new);
        personCriteria.setPersonCriteriaName(personCriteriaDto.getPersonCriteriaName());
        personCriteria.setCotationSet(personCriteriaDto.getCotationDtoSet().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toSet()));
        personCriteriaRepository.save(personCriteria);
    }

    public void deleteById(int personCriteriaDtoId){
        personCriteriaRepository.deleteById(personCriteriaDtoId);
    }
}
