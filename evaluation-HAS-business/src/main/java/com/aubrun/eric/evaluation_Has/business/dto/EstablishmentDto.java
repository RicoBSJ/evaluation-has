package com.aubrun.eric.evaluation_Has.business.dto;

import com.aubrun.eric.evaluation_Has.models.EEstablishment;

public class EstablishmentDto {

    private Integer establishmentId;
    private EEstablishment establishmentName;

    public Integer getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(Integer establishmentId) {
        this.establishmentId = establishmentId;
    }

    public EEstablishment getEstablishmentName() {
        return establishmentName;
    }

    public void setEstablishmentName(EEstablishment establishmentName) {
        this.establishmentName = establishmentName;
    }
}
