package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.EvaluationHASUserDto;
import com.aubrun.eric.evaluation_Has.business.mapper.EvaluationHASUserDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.CustomerRepository;
import com.aubrun.eric.evaluation_Has.consumer.EstablishmentRepository;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.consumer.RoleRepository;
import com.aubrun.eric.evaluation_Has.models.Customer;
import com.aubrun.eric.evaluation_Has.models.Establishment;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.Role;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EvaluationHasUserService {
    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final CustomerRepository customerRepository;
    private final EstablishmentRepository establishmentRepository;
    private final RoleRepository roleRepository;

    public EvaluationHasUserService(EvaluationHasUserRepository evaluationHasUserRepository, CustomerRepository customerRepository, EstablishmentRepository establishmentRepository, RoleRepository roleRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.customerRepository = customerRepository;
        this.establishmentRepository = establishmentRepository;
        this.roleRepository = roleRepository;
    }

    public List<EvaluationHASUserDto> findAll(){
        return evaluationHasUserRepository.findAll().stream().map(EvaluationHASUserDtoMapper::toDto).collect(Collectors.toList());
    }

    public EvaluationHASUserDto findById(int id){
        return EvaluationHASUserDtoMapper.toDto(evaluationHasUserRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    public void save(EvaluationHASUserDto newUser){
        EvaluationHASUser evaluationHASUser = new EvaluationHASUser();
        evaluationHASUser.setFirstName(newUser.getFirstName());
        evaluationHASUser.setLastName(newUser.getLastName());
        evaluationHASUser.setDateBirthUser(newUser.getDateBirthUser());
        evaluationHASUser.setSocialSecurityNumberUser(newUser.getSocialSecurityNumberUser());
        evaluationHASUser.setPhoneUser(newUser.getPhoneUser());
        evaluationHASUser.setEntryDateUser(newUser.getEntryDateUser());
        evaluationHASUser.setAgeUser(newUser.getAgeUser());
        evaluationHASUser.setPassword(newUser.getPassword());
        evaluationHASUser.setEmail(newUser.getEmail());
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void delete(int id){
        evaluationHasUserRepository.deleteById(id);
    }

    public void updateUser(int evaluationHasUserId, LocalDateTime releaseDateUser){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        evaluationHASUser.setReleaseDateUser(releaseDateUser);
        List<Customer> customers = evaluationHASUser.getCustomerList().stream().peek(c -> c.setEvaluationHASUser(null)).toList();
        evaluationHASUser.setCustomerList(new ArrayList<>());
        evaluationHasUserRepository.save(evaluationHASUser);
        customerRepository.saveAll(customers);
    }

    public void addEstablishment(int evaluationHasUserId, List<Integer> establishment){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        List<Establishment> establishmentList = establishmentRepository.findAllById(establishment);
        evaluationHASUser.getEstablishments().addAll(establishmentList);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void addRole(int evaluationHasUserId, List<Integer> roles){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        List<Role> roleList = roleRepository.findAllById(roles);
        evaluationHASUser.getRoles().addAll(roleList);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void deleteEstablishment(int evaluationHasUserId){
        evaluationHasUserRepository.delEstablishment(evaluationHasUserId);
    }

    public void deleteRole(int evaluationHasUserId){
        evaluationHasUserRepository.delRole(evaluationHasUserId);
    }
}
