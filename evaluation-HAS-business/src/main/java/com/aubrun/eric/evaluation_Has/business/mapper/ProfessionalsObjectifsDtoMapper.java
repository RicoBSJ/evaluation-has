package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsObjectifsDto;
import com.aubrun.eric.evaluation_Has.models.ProfessionalsObjectifs;

import java.util.stream.Collectors;

public class ProfessionalsObjectifsDtoMapper {

    static public ProfessionalsObjectifsDto toDto(ProfessionalsObjectifs professionalsObjectifs){

        ProfessionalsObjectifsDto dto = new ProfessionalsObjectifsDto();
        dto.setProfessionalsObjectifsId(professionalsObjectifs.getProfessionalsObjectifsId());
        dto.setProfessionalsObjectifsName(professionalsObjectifs.getProfessionalsObjectifsName());
        dto.setProfessionalsCriteriaList(professionalsObjectifs.getProfessionalsCriteriaList().stream().map(ProfessionalsCriteriaDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }

    static public ProfessionalsObjectifs entity(ProfessionalsObjectifsDto professionalsObjectifsDto){

        ProfessionalsObjectifs entity = new ProfessionalsObjectifs();
        entity.setProfessionalsObjectifsId(professionalsObjectifsDto.getProfessionalsObjectifsId());
        entity.setProfessionalsObjectifsName(professionalsObjectifsDto.getProfessionalsObjectifsName());
        entity.setProfessionalsCriteriaList(professionalsObjectifsDto.getProfessionalsCriteriaList().stream().map(ProfessionalsCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        return entity;
    }
}
