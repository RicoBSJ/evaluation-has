package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.List;

public class ESMSThemesDto {

    private Integer esmsThemesId;
    private String esmsThemeName;
    private List<ESMSObjectifsDto> esmsObjectifsDtoList;

    public Integer getEsmsThemesId() {
        return esmsThemesId;
    }

    public void setEsmsThemesId(Integer esmsThemesId) {
        this.esmsThemesId = esmsThemesId;
    }

    public String getEsmsThemeName() {
        return esmsThemeName;
    }

    public void setEsmsThemeName(String esmsThemeName) {
        this.esmsThemeName = esmsThemeName;
    }

    public List<ESMSObjectifsDto> getEsmsObjectifsDtoList() {
        return esmsObjectifsDtoList;
    }

    public void setEsmsObjectifsDtoList(List<ESMSObjectifsDto> esmsObjectifsDtoList) {
        this.esmsObjectifsDtoList = esmsObjectifsDtoList;
    }
}
