package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSObjectifsDto;
import com.aubrun.eric.evaluation_Has.models.ESMSObjectifs;

import java.util.stream.Collectors;

public class ESMSObjectifsDtoMapper {

    static public ESMSObjectifsDto toDto(ESMSObjectifs esmsObjectifs){

        ESMSObjectifsDto dto = new ESMSObjectifsDto();
        dto.setEsmsObjectifsId(esmsObjectifs.getEsmsObjectifsId());
        dto.setEsmsObjectifName(esmsObjectifs.getEsmsObjectifName());
        dto.setEsmsCriteriaDtoList(esmsObjectifs.getEsmsCriteriaList().stream().map(ESMSCriteriaDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }

    static public ESMSObjectifs toEntity(ESMSObjectifsDto esmsObjectifsDto){

        ESMSObjectifs entity = new ESMSObjectifs();
        entity.setEsmsObjectifsId(esmsObjectifsDto.getEsmsObjectifsId());
        entity.setEsmsObjectifName(esmsObjectifsDto.getEsmsObjectifName());
        entity.setEsmsCriteriaList(esmsObjectifsDto.getEsmsCriteriaDtoList().stream().map(ESMSCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        return entity;
    }
}
