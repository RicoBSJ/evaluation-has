package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.List;

public class ProfessionalsCriteriaDto {

    private Integer professionalsCriteriaId;
    private String professionalsCriteriaName;
    private List<CotationDto> cotationDtoList;

    public Integer getProfessionalsCriteriaId() {
        return professionalsCriteriaId;
    }

    public void setProfessionalsCriteriaId(Integer professionalsCriteriaId) {
        this.professionalsCriteriaId = professionalsCriteriaId;
    }

    public String getProfessionalsCriteriaName() {
        return professionalsCriteriaName;
    }

    public void setProfessionalsCriteriaName(String professionalsCriteriaName) {
        this.professionalsCriteriaName = professionalsCriteriaName;
    }

    public List<CotationDto> getCotationDtoList() {
        return cotationDtoList;
    }

    public void setCotationDtoList(List<CotationDto> cotationDtoList) {
        this.cotationDtoList = cotationDtoList;
    }
}
