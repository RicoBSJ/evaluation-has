package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.EstablishmentDto;
import com.aubrun.eric.evaluation_Has.models.Establishment;

public class EstablishmentDtoMapper {

    static public EstablishmentDto toDto(Establishment establishment) {
        if (establishment == null) return null;
        EstablishmentDto dto = new EstablishmentDto();
        dto.setEstablishmentId(establishment.getEstablishmentId());
        dto.setEstablishmentName(establishment.getEstablishmentName());
        return dto;
    }

    static public Establishment toEntity(EstablishmentDto establishmentDto) {
        Establishment entity = new Establishment();
        entity.setEstablishmentId(establishmentDto.getEstablishmentId());
        entity.setEstablishmentName(establishmentDto.getEstablishmentName());
        return entity;
    }
}
