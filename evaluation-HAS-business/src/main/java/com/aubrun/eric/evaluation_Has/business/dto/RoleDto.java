package com.aubrun.eric.evaluation_Has.business.dto;

import com.aubrun.eric.evaluation_Has.models.ERole;

public class RoleDto {

    private Integer roleId;
    private ERole roleName;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public ERole getRoleName() {
        return roleName;
    }

    public void setRoleName(ERole roleName) {
        this.roleName = roleName;
    }
}
