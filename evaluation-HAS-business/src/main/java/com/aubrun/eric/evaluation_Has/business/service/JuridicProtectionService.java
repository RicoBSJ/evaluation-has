package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.JuridicProtectionDto;
import com.aubrun.eric.evaluation_Has.business.mapper.JuridicProtectionDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.consumer.JuridicProtectionRepository;
import com.aubrun.eric.evaluation_Has.models.Cotation;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.JuridicProtection;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class JuridicProtectionService {

    private final JuridicProtectionRepository juridicProtectionRepository;

    private final EvaluationHasUserRepository evaluationHasUserRepository;

    public JuridicProtectionService(JuridicProtectionRepository juridicProtectionRepository, EvaluationHasUserRepository evaluationHasUserRepository) {
        this.juridicProtectionRepository = juridicProtectionRepository;
        this.evaluationHasUserRepository = evaluationHasUserRepository;
    }

    public List<JuridicProtectionDto> findAll() {
        return juridicProtectionRepository.findAll().stream().map(JuridicProtectionDtoMapper::toDto).collect(Collectors.toList());
    }

    public JuridicProtectionDto findById(int juridicProtectionId) {
        return JuridicProtectionDtoMapper.toDto(juridicProtectionRepository.findById(juridicProtectionId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, JuridicProtectionDto juridicProtectionDto) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        JuridicProtection cotation = new JuridicProtection();
        cotation.setJuridicProtectionName(juridicProtectionDto.getJuridicProtectionName());
        juridicProtectionRepository.save(cotation);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(JuridicProtectionDto juridicProtectionDto, int juridicProtectionDtoId) {
        JuridicProtection juridicProtection = juridicProtectionRepository.findById(juridicProtectionDtoId).orElseThrow(RuntimeException::new);
        juridicProtection.setJuridicProtectionName(juridicProtectionDto.getJuridicProtectionName());
        juridicProtectionRepository.save(juridicProtection);
    }

    public void deleteById(int juridicProtectionId){
        juridicProtectionRepository.deleteById(juridicProtectionId);
    }
}
