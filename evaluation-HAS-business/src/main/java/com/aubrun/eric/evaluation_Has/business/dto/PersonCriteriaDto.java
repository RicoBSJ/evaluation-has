package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.Set;

public class PersonCriteriaDto {

    private Integer personCriteriaId;
    private String personCriteriaName;
    private Set<CotationDto> cotationDtoSet;

    public Integer getPersonCriteriaId() {
        return personCriteriaId;
    }

    public void setPersonCriteriaId(Integer personCriteriaId) {
        this.personCriteriaId = personCriteriaId;
    }

    public String getPersonCriteriaName() {
        return personCriteriaName;
    }

    public void setPersonCriteriaName(String personCriteriaName) {
        this.personCriteriaName = personCriteriaName;
    }

    public Set<CotationDto> getCotationDtoSet() {
        return cotationDtoSet;
    }

    public void setCotationDtoSet(Set<CotationDto> cotationDtoSet) {
        this.cotationDtoSet = cotationDtoSet;
    }
}
