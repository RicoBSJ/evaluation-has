package com.aubrun.eric.evaluation_Has.business.dto;

import javax.persistence.*;
import java.util.List;

public class ProfessionalsObjectifsDto {

    private Integer professionalsObjectifsId;
    private String professionalsObjectifsName;
    private List<ProfessionalsCriteriaDto> professionalsCriteriaList;

    public Integer getProfessionalsObjectifsId() {
        return professionalsObjectifsId;
    }

    public void setProfessionalsObjectifsId(Integer professionalsObjectifsId) {
        this.professionalsObjectifsId = professionalsObjectifsId;
    }

    public String getProfessionalsObjectifsName() {
        return professionalsObjectifsName;
    }

    public void setProfessionalsObjectifsName(String professionalsObjectifsName) {
        this.professionalsObjectifsName = professionalsObjectifsName;
    }

    public List<ProfessionalsCriteriaDto> getProfessionalsCriteriaList() {
        return professionalsCriteriaList;
    }

    public void setProfessionalsCriteriaList(List<ProfessionalsCriteriaDto> professionalsCriteriaList) {
        this.professionalsCriteriaList = professionalsCriteriaList;
    }
}
