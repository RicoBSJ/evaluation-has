package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSObjectifsDto;
import com.aubrun.eric.evaluation_Has.business.mapper.ESMSCriteriaDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.ESMSObjectifsDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.ESMSObjectifsRepository;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.models.ESMSObjectifs;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ESMSObjectifsService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final ESMSObjectifsRepository esmsObjectifsRepository;

    public ESMSObjectifsService(EvaluationHasUserRepository evaluationHasUserRepository, ESMSObjectifsRepository esmsObjectifsRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.esmsObjectifsRepository = esmsObjectifsRepository;
    }

    public List<ESMSObjectifsDto> findAll(){
        return esmsObjectifsRepository.findAll().stream().map(ESMSObjectifsDtoMapper::toDto).collect(Collectors.toList());
    }

    public ESMSObjectifsDto findById(int esmsObjectifsId){
        return ESMSObjectifsDtoMapper.toDto(esmsObjectifsRepository.findById(esmsObjectifsId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, ESMSObjectifsDto esmsObjectifsDto){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        ESMSObjectifs esmsObjectifs = new ESMSObjectifs();
        esmsObjectifs.setEsmsObjectifName(esmsObjectifsDto.getEsmsObjectifName());
        esmsObjectifs.setEsmsCriteriaList(esmsObjectifsDto.getEsmsCriteriaDtoList().stream().map(ESMSCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        esmsObjectifsRepository.save(esmsObjectifs);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(ESMSObjectifsDto esmsObjectifsDto, int esmsObjectifsDtoId){
        ESMSObjectifs esmsObjectifs = esmsObjectifsRepository.findById(esmsObjectifsDtoId).orElseThrow(RuntimeException::new);
        esmsObjectifs.setEsmsObjectifName(esmsObjectifsDto.getEsmsObjectifName());
        esmsObjectifs.setEsmsCriteriaList(esmsObjectifsDto.getEsmsCriteriaDtoList().stream().map(ESMSCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        esmsObjectifsRepository.save(esmsObjectifs);
    }

    public void deleteById(int esmsObjectifsDtoId){
        esmsObjectifsRepository.deleteById(esmsObjectifsDtoId);
    }
}
