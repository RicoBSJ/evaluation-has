package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.PersonThemesDto;
import com.aubrun.eric.evaluation_Has.business.mapper.PersonObjectifsDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.PersonThemesDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.consumer.PersonThemesRepository;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.PersonThemes;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonThemesService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final PersonThemesRepository personThemesRepository;

    public PersonThemesService(EvaluationHasUserRepository evaluationHasUserRepository, PersonThemesRepository personThemesRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.personThemesRepository = personThemesRepository;
    }

    public List<PersonThemesDto> findAll(){
        return personThemesRepository.findAll().stream().map(PersonThemesDtoMapper::toDto).collect(Collectors.toList());
    }

    public PersonThemesDto findById(int personThemesId){
        return PersonThemesDtoMapper.toDto(personThemesRepository.findById(personThemesId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, PersonThemesDto personThemesDto){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        PersonThemes personThemes = new PersonThemes();
        personThemes.setPersonThemeName(personThemesDto.getPersonThemeName());
        personThemes.setPersonObjectifsList(personThemesDto.getPersonObjectifsDtoList().stream().map(PersonObjectifsDtoMapper::toEntity).collect(Collectors.toList()));
        personThemesRepository.save(personThemes);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(PersonThemesDto personThemesDto, int personDtoId){
        PersonThemes personThemes = personThemesRepository.findById(personDtoId).orElseThrow(RuntimeException::new);
        personThemes.setPersonThemeName(personThemesDto.getPersonThemeName());
        personThemes.setPersonObjectifsList(personThemesDto.getPersonObjectifsDtoList().stream().map(PersonObjectifsDtoMapper::toEntity).collect(Collectors.toList()));
        personThemesRepository.save(personThemes);
    }

    public void deleteById(int personDtoId){
        personThemesRepository.deleteById(personDtoId);
    }
}
