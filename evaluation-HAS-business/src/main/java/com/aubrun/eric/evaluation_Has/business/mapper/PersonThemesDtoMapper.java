package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.PersonThemesDto;
import com.aubrun.eric.evaluation_Has.models.PersonThemes;

import java.util.stream.Collectors;

public class PersonThemesDtoMapper {

    static public PersonThemesDto toDto(PersonThemes personThemes) {

        PersonThemesDto dto = new PersonThemesDto();
        dto.setPersonThemesId(personThemes.getPersonThemesId());
        dto.setPersonThemeName(personThemes.getPersonThemeName());
        dto.setPersonObjectifsDtoList(personThemes.getPersonObjectifsList().stream().map(PersonObjectifsDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }

    static public PersonThemes toEntity(PersonThemesDto personThemesDto) {

        PersonThemes entity = new PersonThemes();
        entity.setPersonThemesId(personThemesDto.getPersonThemesId());
        entity.setPersonThemeName(personThemesDto.getPersonThemeName());
        entity.setPersonObjectifsList(personThemesDto.getPersonObjectifsDtoList().stream().map(PersonObjectifsDtoMapper::toEntity).collect(Collectors.toList()));
        return entity;
    }
}
