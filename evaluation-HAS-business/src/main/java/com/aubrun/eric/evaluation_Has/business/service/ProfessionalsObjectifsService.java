package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsObjectifsDto;
import com.aubrun.eric.evaluation_Has.business.mapper.ProfessionalsCriteriaDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.ProfessionalsObjectifsDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.consumer.ProfessionalsObjectifsRepository;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.ProfessionalsObjectifs;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProfessionalsObjectifsService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final ProfessionalsObjectifsRepository professionalsObjectifsRepository;

    public ProfessionalsObjectifsService(EvaluationHasUserRepository evaluationHasUserRepository, ProfessionalsObjectifsRepository professionalsObjectifsRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.professionalsObjectifsRepository = professionalsObjectifsRepository;
    }

    public List<ProfessionalsObjectifsDto> findAll(){
        return professionalsObjectifsRepository.findAll().stream().map(ProfessionalsObjectifsDtoMapper::toDto).collect(Collectors.toList());
    }

    public ProfessionalsObjectifsDto findById(int professionalsObjectifsId){
        return ProfessionalsObjectifsDtoMapper.toDto(professionalsObjectifsRepository.findById(professionalsObjectifsId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, ProfessionalsObjectifsDto professionalsObjectifsDto){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        ProfessionalsObjectifs professionalsObjectifs = new ProfessionalsObjectifs();
        professionalsObjectifs.setProfessionalsObjectifsName(professionalsObjectifsDto.getProfessionalsObjectifsName());
        professionalsObjectifs.setProfessionalsCriteriaList(professionalsObjectifsDto.getProfessionalsCriteriaList().stream().map(ProfessionalsCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        professionalsObjectifsRepository.save(professionalsObjectifs);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(ProfessionalsObjectifsDto professionalsObjectifsDto, int professionalsObjectifsDtoId){
        ProfessionalsObjectifs professionalsObjectifs = professionalsObjectifsRepository.findById(professionalsObjectifsDtoId).orElseThrow(RuntimeException::new);
        professionalsObjectifs.setProfessionalsObjectifsName(professionalsObjectifsDto.getProfessionalsObjectifsName());
        professionalsObjectifs.setProfessionalsCriteriaList(professionalsObjectifsDto.getProfessionalsCriteriaList().stream().map(ProfessionalsCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        professionalsObjectifsRepository.save(professionalsObjectifs);
    }

    public void deleteById(int professionalsObjectifsDtoId){
        professionalsObjectifsRepository.deleteById(professionalsObjectifsDtoId);
    }
}
