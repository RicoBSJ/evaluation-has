package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.PersonObjectifsDto;
import com.aubrun.eric.evaluation_Has.models.PersonObjectifs;

import java.util.stream.Collectors;

public class PersonObjectifsDtoMapper {

    static public PersonObjectifsDto toDto(PersonObjectifs personObjectifs){

        PersonObjectifsDto dto = new PersonObjectifsDto();
        dto.setPersonObjectifsId(personObjectifs.getPersonObjectifsId());
        dto.setPersonObjectifName(personObjectifs.getPersonObjectifName());
        dto.setPersonCriteriaDtoList(personObjectifs.getPersonCriteriaList().stream().map(PersonCriteriaDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }

    static public PersonObjectifs toEntity(PersonObjectifsDto personObjectifsDto){

        PersonObjectifs entity = new PersonObjectifs();
        entity.setPersonObjectifsId(personObjectifsDto.getPersonObjectifsId());
        entity.setPersonObjectifName(personObjectifsDto.getPersonObjectifName());
        entity.setPersonCriteriaList(personObjectifsDto.getPersonCriteriaDtoList().stream().map(PersonCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        return entity;
    }
}
