package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSThemesDto;
import com.aubrun.eric.evaluation_Has.models.ESMSThemes;

import java.util.stream.Collectors;

public class ESMSThemesDtoMapper {

    static public ESMSThemesDto toDto(ESMSThemes esmsThemes){

        ESMSThemesDto dto = new ESMSThemesDto();
        dto.setEsmsThemesId(esmsThemes.getEsmsThemesId());
        dto.setEsmsThemeName(esmsThemes.getEsmsThemeName());
        dto.setEsmsObjectifsDtoList(esmsThemes.getEsmsObjectifsList().stream().map(ESMSObjectifsDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }

    static public ESMSThemes toEntity(ESMSThemesDto esmsThemesDto){

        ESMSThemes entity = new ESMSThemes();
        entity.setEsmsThemesId(esmsThemesDto.getEsmsThemesId());
        entity.setEsmsThemeName(esmsThemesDto.getEsmsThemeName());
        entity.setEsmsObjectifsList(esmsThemesDto.getEsmsObjectifsDtoList().stream().map(ESMSObjectifsDtoMapper::toEntity).collect(Collectors.toList()));
        return entity;
    }
}
