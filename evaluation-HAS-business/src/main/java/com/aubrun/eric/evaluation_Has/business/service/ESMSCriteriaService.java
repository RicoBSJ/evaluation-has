package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSCriteriaDto;
import com.aubrun.eric.evaluation_Has.business.mapper.CotationDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.ESMSCriteriaDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.ESMSCriteriaRepository;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.models.ESMSCriteria;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ESMSCriteriaService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final ESMSCriteriaRepository esmsCriteriaRepository;

    public ESMSCriteriaService(EvaluationHasUserRepository evaluationHasUserRepository, ESMSCriteriaRepository esmsCriteriaRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.esmsCriteriaRepository = esmsCriteriaRepository;
    }

    public List<ESMSCriteriaDto> findAll(){
        return esmsCriteriaRepository.findAll().stream().map(ESMSCriteriaDtoMapper::toDto).collect(Collectors.toList());
    }

    public ESMSCriteriaDto findById(int esmsCriteriaId){
        return ESMSCriteriaDtoMapper.toDto(esmsCriteriaRepository.findById(esmsCriteriaId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, ESMSCriteriaDto esmsCriteriaDto){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        ESMSCriteria esmsCriteria = new ESMSCriteria();
        esmsCriteria.setEsmsCriteriaName(esmsCriteriaDto.getEsmsCriteriaName());
        esmsCriteria.setCotationSet(esmsCriteriaDto.getCotationDtoSet().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toSet()));
        esmsCriteriaRepository.save(esmsCriteria);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(ESMSCriteriaDto esmsCriteriaDto, int esmsCriteriaDtoId){
        ESMSCriteria esmsCriteria = esmsCriteriaRepository.findById(esmsCriteriaDtoId).orElseThrow(RuntimeException::new);
        esmsCriteria.setEsmsCriteriaName(esmsCriteriaDto.getEsmsCriteriaName());
        esmsCriteria.setCotationSet(esmsCriteriaDto.getCotationDtoSet().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toSet()));
        esmsCriteriaRepository.save(esmsCriteria);
    }

    public void deleteById(int esmsCriteriaDtoId){
        esmsCriteriaRepository.deleteById(esmsCriteriaDtoId);
    }
}
