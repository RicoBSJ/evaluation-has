package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.List;

public class PersonObjectifsDto {

    private Integer personObjectifsId;
    private String personObjectifName;
    private List<PersonCriteriaDto> personCriteriaDtoList;

    public Integer getPersonObjectifsId() {
        return personObjectifsId;
    }

    public void setPersonObjectifsId(Integer personObjectifsId) {
        this.personObjectifsId = personObjectifsId;
    }

    public String getPersonObjectifName() {
        return personObjectifName;
    }

    public void setPersonObjectifName(String personObjectifName) {
        this.personObjectifName = personObjectifName;
    }

    public List<PersonCriteriaDto> getPersonCriteriaDtoList() {
        return personCriteriaDtoList;
    }

    public void setPersonCriteriaDtoList(List<PersonCriteriaDto> personCriteriaDtoList) {
        this.personCriteriaDtoList = personCriteriaDtoList;
    }
}
