package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.Collections;
import java.util.List;

public class AddPersonThemesDto {

    private List<Integer> personThemes;

    public AddPersonThemesDto(int personThemes) {
        this.personThemes = Collections.singletonList(personThemes);
    }

    public List<Integer> getPersonThemes() {
        return personThemes;
    }

    public void setPersonThemes(List<Integer> personThemes) {
        this.personThemes = personThemes;
    }
}
