package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSThemesDto;
import com.aubrun.eric.evaluation_Has.business.mapper.ESMSObjectifsDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.ESMSThemesDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.ESMSThemesRepository;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.models.ESMSThemes;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ESMSThemesService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final ESMSThemesRepository esmsThemesRepository;

    public ESMSThemesService(EvaluationHasUserRepository evaluationHasUserRepository, ESMSThemesRepository esmsThemesRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.esmsThemesRepository = esmsThemesRepository;
    }

    public List<ESMSThemesDto> findAll(){
        return esmsThemesRepository.findAll().stream().map(ESMSThemesDtoMapper::toDto).collect(Collectors.toList());
    }

    public ESMSThemesDto findById(int esmsThemesId){
        return ESMSThemesDtoMapper.toDto(esmsThemesRepository.findById(esmsThemesId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, ESMSThemesDto esmsThemesDto){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        ESMSThemes esmsThemes = new ESMSThemes();
        esmsThemes.setEsmsThemeName(esmsThemesDto.getEsmsThemeName());
        esmsThemes.setEsmsObjectifsList(esmsThemesDto.getEsmsObjectifsDtoList().stream().map(ESMSObjectifsDtoMapper::toEntity).collect(Collectors.toList()));
        esmsThemesRepository.save(esmsThemes);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(ESMSThemesDto esmsThemesDto, int esmsThemesDtoId){
        ESMSThemes esmsThemes = esmsThemesRepository.findById(esmsThemesDtoId).orElseThrow(RuntimeException::new);
        esmsThemes.setEsmsThemeName(esmsThemesDto.getEsmsThemeName());
        esmsThemes.setEsmsObjectifsList(esmsThemesDto.getEsmsObjectifsDtoList().stream().map(ESMSObjectifsDtoMapper::toEntity).collect(Collectors.toList()));
        esmsThemesRepository.save(esmsThemes);
    }

    public void deleteById(int esmsThemesDtoId){
        esmsThemesRepository.deleteById(esmsThemesDtoId);
    }
}
