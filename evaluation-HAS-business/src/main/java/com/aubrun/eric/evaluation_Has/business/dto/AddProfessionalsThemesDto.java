package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.Collections;
import java.util.List;

public class AddProfessionalsThemesDto {

    private List<Integer> professionalsThemes;

    public AddProfessionalsThemesDto(int professionalsThemes) {
        this.professionalsThemes = Collections.singletonList(professionalsThemes);
    }

    public List<Integer> getProfessionalsThemes() {
        return professionalsThemes;
    }

    public void setProfessionalsThemes(List<Integer> professionalsThemes) {
        this.professionalsThemes = professionalsThemes;
    }
}
