package com.aubrun.eric.evaluation_Has.business.dto;

import com.aubrun.eric.evaluation_Has.models.ECotation;

public class CotationDto {

    private Integer cotationId;
    private ECotation eCotation;

    public Integer getCotationId() {
        return cotationId;
    }

    public void setCotationId(Integer cotationId) {
        this.cotationId = cotationId;
    }

    public ECotation geteCotation() {
        return eCotation;
    }

    public void seteCotation(ECotation eCotation) {
        this.eCotation = eCotation;
    }
}
