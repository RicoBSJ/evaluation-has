package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.List;

public class ESMSObjectifsDto {

    private Integer esmsObjectifsId;
    private String esmsObjectifName;
    private List<ESMSCriteriaDto> esmsCriteriaDtoList;

    public Integer getEsmsObjectifsId() {
        return esmsObjectifsId;
    }

    public void setEsmsObjectifsId(Integer esmsObjectifsId) {
        this.esmsObjectifsId = esmsObjectifsId;
    }

    public String getEsmsObjectifName() {
        return esmsObjectifName;
    }

    public void setEsmsObjectifName(String esmsObjectifName) {
        this.esmsObjectifName = esmsObjectifName;
    }

    public List<ESMSCriteriaDto> getEsmsCriteriaDtoList() {
        return esmsCriteriaDtoList;
    }

    public void setEsmsCriteriaDtoList(List<ESMSCriteriaDto> esmsCriteriaDtoList) {
        this.esmsCriteriaDtoList = esmsCriteriaDtoList;
    }
}
