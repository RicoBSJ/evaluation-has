package com.aubrun.eric.evaluation_Has.business.service.exception;

import com.aubrun.eric.evaluation_Has.models.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class WrongIdentifier extends RuntimeException {

    public WrongIdentifier() {
        super("Please verify your login details");
    }


}
