package com.aubrun.eric.evaluation_Has.business.dto;

import com.aubrun.eric.evaluation_Has.models.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.Collections;
import java.util.List;

@ExcludeClassFromJacocoGeneratedReport
public class AddRoleDto {

    private List<Integer> roles;

    public AddRoleDto(int roles) {
        this.roles = Collections.singletonList(roles);
    }

    public List<Integer> getRoles() {
        return roles;
    }

    public void setRoles(List<Integer> roles) {
        this.roles = roles;
    }
}
