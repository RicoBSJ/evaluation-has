package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsThemesDto;
import com.aubrun.eric.evaluation_Has.models.ProfessionalsThemes;

import java.util.stream.Collectors;

public class ProfessionalsThemesDtoMapper {

    static public ProfessionalsThemesDto toDto(ProfessionalsThemes professionalsThemes){

        ProfessionalsThemesDto dto = new ProfessionalsThemesDto();
        dto.setProfessionalsThemesId(professionalsThemes.getProfessionalsThemesId());
        dto.setProfessionalsThemeName(professionalsThemes.getProfessionalsThemeName());
        dto.setProfessionalsObjectifsDtoList(professionalsThemes.getProfessionalsObjectifsList().stream().map(ProfessionalsObjectifsDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }

    static public ProfessionalsThemes toEntity(ProfessionalsThemesDto professionalsThemesDto){

        ProfessionalsThemes entity = new ProfessionalsThemes();
        entity.setProfessionalsThemesId(professionalsThemesDto.getProfessionalsThemesId());
        entity.setProfessionalsThemeName(professionalsThemesDto.getProfessionalsThemeName());
        entity.setProfessionalsObjectifsList(professionalsThemesDto.getProfessionalsObjectifsDtoList().stream().map(ProfessionalsObjectifsDtoMapper::entity).collect(Collectors.toList()));
        return entity;
    }
}
