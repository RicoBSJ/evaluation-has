package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsCriteriaDto;
import com.aubrun.eric.evaluation_Has.business.mapper.CotationDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.ProfessionalsCriteriaDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.CustomerRepository;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.consumer.ProfessionalsCriteriaRepository;
import com.aubrun.eric.evaluation_Has.models.Customer;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.ProfessionalsCriteria;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProfessionalsCriteriaService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final CustomerRepository customerRepository;
    private final ProfessionalsCriteriaRepository professionalsCriteriaRepository;

    public ProfessionalsCriteriaService(EvaluationHasUserRepository evaluationHasUserRepository, CustomerRepository customerRepository, ProfessionalsCriteriaRepository professionalsCriteriaRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.customerRepository = customerRepository;
        this.professionalsCriteriaRepository = professionalsCriteriaRepository;
    }

    public List<ProfessionalsCriteriaDto> findAll(){
        return professionalsCriteriaRepository.findAll().stream().map(ProfessionalsCriteriaDtoMapper::toDto).collect(Collectors.toList());
    }

    public ProfessionalsCriteriaDto findById(int professionalsCriteriaId){
        return ProfessionalsCriteriaDtoMapper.toDto(professionalsCriteriaRepository.findById(professionalsCriteriaId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, ProfessionalsCriteriaDto professionalsCriteriaDto, int customerId){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        ProfessionalsCriteria professionalsCriteria = new ProfessionalsCriteria();
        professionalsCriteria.setProfessionalsCriteriaName(professionalsCriteriaDto.getProfessionalsCriteriaName());
        professionalsCriteria.setCotationList(professionalsCriteriaDto.getCotationDtoList().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toList()));
        professionalsCriteriaRepository.save(professionalsCriteria);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(ProfessionalsCriteriaDto professionalsCriteriaDto, int customerId){
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        ProfessionalsCriteria professionalsCriteria = professionalsCriteriaRepository.findById(professionalsCriteriaDto.getProfessionalsCriteriaId()).orElseThrow(RuntimeException::new);
        professionalsCriteria.setProfessionalsCriteriaName(professionalsCriteriaDto.getProfessionalsCriteriaName());
        professionalsCriteria.setCotationList(professionalsCriteriaDto.getCotationDtoList().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toList()));
        professionalsCriteriaRepository.save(professionalsCriteria);
    }

    public void deleteById(int professionalsCriteriaDtoId){
        professionalsCriteriaRepository.deleteById(professionalsCriteriaDtoId);
    }
}
