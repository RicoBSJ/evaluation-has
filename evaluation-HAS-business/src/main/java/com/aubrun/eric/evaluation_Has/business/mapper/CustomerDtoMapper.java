package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.CustomerDto;
import com.aubrun.eric.evaluation_Has.models.Customer;

import java.util.stream.Collectors;

public class CustomerDtoMapper {

    static public CustomerDto toDto(Customer customer) {

        CustomerDto dto = new CustomerDto();
        dto.setCustomerId(customer.getCustomerId());
        dto.setCustomerFirstName(customer.getCustomerFirstName());
        dto.setCustomerLastName(customer.getCustomerLastName());
        dto.setEvaluationHASUser(customer.getEvaluationHASUser() == null ? "" :customer.getEvaluationHASUser().getFirstName()+" "+customer.getEvaluationHASUser().getLastName());
        dto.setAge(customer.getAge());
        dto.setDateBirth(customer.getDateBirth());
        dto.setSocialSecurityNumber(customer.getSocialSecurityNumber());
        dto.setMutualName(customer.getMutualName());
        dto.setEntryDate(customer.getEntryDate());
        dto.setReleaseDate(customer.getReleaseDate());
        dto.setJuridicProtectionDtoSet(customer.getJuridicProtectionSet().stream().map(JuridicProtectionDtoMapper::toDto).collect(Collectors.toSet()));
        dto.setEstablishmentDtoSet(customer.getEstablishmentList().stream().map(EstablishmentDtoMapper::toDto).collect(Collectors.toSet()));
        dto.setPersonThemesDtoList(customer.getPersonThemesList().stream().map(PersonThemesDtoMapper::toDto).collect(Collectors.toList()));
        dto.setProfessionalsThemesDtoList(customer.getProfessionalsThemesList().stream().map(ProfessionalsThemesDtoMapper::toDto).collect(Collectors.toList()));
        dto.setEsmsThemesDtoList(customer.getEsmsThemesList().stream().map(ESMSThemesDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }
}
