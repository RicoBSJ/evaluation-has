package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsThemesDto;
import com.aubrun.eric.evaluation_Has.business.mapper.ProfessionalsObjectifsDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.ProfessionalsThemesDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.consumer.ProfessionalsThemesRepository;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.ProfessionalsThemes;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProfessionalsThemesService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final ProfessionalsThemesRepository professionalsThemesRepository;

    public ProfessionalsThemesService(EvaluationHasUserRepository evaluationHasUserRepository, ProfessionalsThemesRepository professionalsThemesRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.professionalsThemesRepository = professionalsThemesRepository;
    }

    public List<ProfessionalsThemesDto> findAll(){
        return professionalsThemesRepository.findAll().stream().map(ProfessionalsThemesDtoMapper::toDto).collect(Collectors.toList());
    }

    public ProfessionalsThemesDto findById(int professionalsThemesId){
        return ProfessionalsThemesDtoMapper.toDto(professionalsThemesRepository.findById(professionalsThemesId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, ProfessionalsThemesDto professionalsThemesDto){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        ProfessionalsThemes professionalsThemes = new ProfessionalsThemes();
        professionalsThemes.setProfessionalsThemeName(professionalsThemesDto.getProfessionalsThemeName());
        professionalsThemes.setProfessionalsObjectifsList(professionalsThemesDto.getProfessionalsObjectifsDtoList().stream().map(ProfessionalsObjectifsDtoMapper::entity).collect(Collectors.toList()));
        professionalsThemesRepository.save(professionalsThemes);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(ProfessionalsThemesDto professionalsThemesDto, int professionalsThemesDtoId){
        ProfessionalsThemes professionalsThemes = professionalsThemesRepository.findById(professionalsThemesDtoId).orElseThrow(RuntimeException::new);
        professionalsThemes.setProfessionalsThemeName(professionalsThemesDto.getProfessionalsThemeName());
        professionalsThemes.setProfessionalsObjectifsList(professionalsThemesDto.getProfessionalsObjectifsDtoList().stream().map(ProfessionalsObjectifsDtoMapper::entity).collect(Collectors.toList()));
        professionalsThemesRepository.save(professionalsThemes);
    }

    public void deleteById(int professionalsThemesDtoId){
        professionalsThemesRepository.deleteById(professionalsThemesDtoId);
    }
}
