package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.PersonCriteriaDto;
import com.aubrun.eric.evaluation_Has.models.PersonCriteria;

import java.util.stream.Collectors;

public class PersonCriteriaDtoMapper {

    static public PersonCriteriaDto toDto(PersonCriteria personCriteria){

        PersonCriteriaDto dto = new PersonCriteriaDto();
        dto.setPersonCriteriaId(personCriteria.getPersonCriteriaId());
        dto.setPersonCriteriaName(personCriteria.getPersonCriteriaName());
        dto.setCotationDtoSet(personCriteria.getCotationSet().stream().map(CotationDtoMapper::toDto).collect(Collectors.toSet()));
        return dto;
    }

    static public PersonCriteria toEntity(PersonCriteriaDto personCriteriaDto){

        PersonCriteria entity = new PersonCriteria();
        entity.setPersonCriteriaId(personCriteriaDto.getPersonCriteriaId());
        entity.setPersonCriteriaName(personCriteriaDto.getPersonCriteriaName());
        entity.setCotationSet(personCriteriaDto.getCotationDtoSet().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toSet()));
        return entity;
    }
}
