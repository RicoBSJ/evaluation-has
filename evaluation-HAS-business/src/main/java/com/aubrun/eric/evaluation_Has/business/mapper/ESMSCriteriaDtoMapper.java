package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSCriteriaDto;
import com.aubrun.eric.evaluation_Has.models.ESMSCriteria;

import java.util.stream.Collectors;

public class ESMSCriteriaDtoMapper {

    static public ESMSCriteriaDto toDto(ESMSCriteria esmsCriteria){

        ESMSCriteriaDto dto = new ESMSCriteriaDto();
        dto.setEsmsCriteriaId(esmsCriteria.getEsmsCriteriaId());
        dto.setEsmsCriteriaName(esmsCriteria.getEsmsCriteriaName());
        dto.setCotationDtoSet(esmsCriteria.getCotationSet().stream().map(CotationDtoMapper::toDto).collect(Collectors.toSet()));
        return dto;
    }

    static public ESMSCriteria toEntity(ESMSCriteriaDto esmsCriteriaDto){

        ESMSCriteria entity = new ESMSCriteria();
        entity.setEsmsCriteriaId(esmsCriteriaDto.getEsmsCriteriaId());
        entity.setEsmsCriteriaName(esmsCriteriaDto.getEsmsCriteriaName());
        entity.setCotationSet(esmsCriteriaDto.getCotationDtoSet().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toSet()));
        return entity;
    }
}
