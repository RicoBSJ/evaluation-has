package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.EstablishmentDto;
import com.aubrun.eric.evaluation_Has.business.mapper.EstablishmentDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.EstablishmentRepository;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.models.Establishment;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EstablishmentService {

    private final EstablishmentRepository establishmentRepository;

    private final EvaluationHasUserRepository evaluationHasUserRepository;

    public EstablishmentService(EstablishmentRepository establishmentRepository, EvaluationHasUserRepository evaluationHasUserRepository) {
        this.establishmentRepository = establishmentRepository;
        this.evaluationHasUserRepository = evaluationHasUserRepository;
    }

    public List<EstablishmentDto> findAll() {
        return establishmentRepository.findAll().stream().map(EstablishmentDtoMapper::toDto).collect(Collectors.toList());
    }

    public EstablishmentDto findById(int establishmentId) {
        return EstablishmentDtoMapper.toDto(establishmentRepository.findById(establishmentId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, EstablishmentDto establishmentDto) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        Establishment establishment = new Establishment();
        establishment.setEstablishmentName(establishmentDto.getEstablishmentName());
        establishmentRepository.save(establishment);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(EstablishmentDto establishmentDto, int establishmentDtoId) {
        Establishment establishment = establishmentRepository.findById(establishmentDtoId).orElseThrow(RuntimeException::new);
        establishment.setEstablishmentName(establishmentDto.getEstablishmentName());
        establishmentRepository.save(establishment);
    }

    public void deleteById(int establishmentDtoId){
        establishmentRepository.deleteById(establishmentDtoId);
    }

}
