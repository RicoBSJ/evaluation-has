package com.aubrun.eric.evaluation_Has.business.dto;

import com.aubrun.eric.evaluation_Has.models.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.Collections;
import java.util.List;

@ExcludeClassFromJacocoGeneratedReport
public class AddEstablishmentsDto {

    private List<Integer> establishments;

    public AddEstablishmentsDto(int establishments) {
        this.establishments = Collections.singletonList(establishments);
    }

    public List<Integer> getEstablishments() {
        return establishments;
    }

    public void setEstablishments(List<Integer> establishments) {
        this.establishments = establishments;
    }
}
