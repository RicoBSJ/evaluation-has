package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.CotationDto;
import com.aubrun.eric.evaluation_Has.models.Cotation;

public class CotationDtoMapper {

    static public CotationDto toDto(Cotation cotation){

        CotationDto dto = new CotationDto();
        dto.setCotationId(cotation.getCotationId());
        dto.seteCotation(cotation.geteCotation());
        return dto;
    }

    static public Cotation toEntity(CotationDto cotationDto){

        Cotation entity = new Cotation();
        entity.setCotationId(cotationDto.getCotationId());
        entity.seteCotation(cotationDto.geteCotation());
        return entity;
    }
}
