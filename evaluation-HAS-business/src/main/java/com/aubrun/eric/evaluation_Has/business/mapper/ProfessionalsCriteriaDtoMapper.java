package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsCriteriaDto;
import com.aubrun.eric.evaluation_Has.models.ProfessionalsCriteria;

import java.util.stream.Collectors;

public class ProfessionalsCriteriaDtoMapper {

    static public ProfessionalsCriteriaDto toDto(ProfessionalsCriteria professionalsCriteria){

        ProfessionalsCriteriaDto dto = new ProfessionalsCriteriaDto();
        dto.setProfessionalsCriteriaId(professionalsCriteria.getProfessionalsCriteriaId());
        dto.setProfessionalsCriteriaName(professionalsCriteria.getProfessionalsCriteriaName());
        dto.setCotationDtoList(professionalsCriteria.getCotationList().stream().map(CotationDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }

    static public ProfessionalsCriteria toEntity(ProfessionalsCriteriaDto professionalsCriteriaDto){

        ProfessionalsCriteria entity = new ProfessionalsCriteria();
        entity.setProfessionalsCriteriaId(professionalsCriteriaDto.getProfessionalsCriteriaId());
        entity.setProfessionalsCriteriaName(professionalsCriteriaDto.getProfessionalsCriteriaName());
        entity.setCotationList(professionalsCriteriaDto.getCotationDtoList().stream().map(CotationDtoMapper::toEntity).collect(Collectors.toList()));
        return entity;
    }
}
