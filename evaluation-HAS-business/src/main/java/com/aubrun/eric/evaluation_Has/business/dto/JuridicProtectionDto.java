package com.aubrun.eric.evaluation_Has.business.dto;

import com.aubrun.eric.evaluation_Has.models.EJuridicProtection;

public class JuridicProtectionDto {

    private Integer juridicProtectionId;
    private EJuridicProtection juridicProtectionName;

    public Integer getJuridicProtectionId() {
        return juridicProtectionId;
    }

    public void setJuridicProtectionId(Integer juridicProtectionId) {
        this.juridicProtectionId = juridicProtectionId;
    }

    public EJuridicProtection getJuridicProtectionName() {
        return juridicProtectionName;
    }

    public void setJuridicProtectionName(EJuridicProtection juridicProtectionName) {
        this.juridicProtectionName = juridicProtectionName;
    }
}
