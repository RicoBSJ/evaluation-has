package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.Set;

public class ESMSCriteriaDto {

    private Integer esmsCriteriaId;
    private String esmsCriteriaName;
    private Set<CotationDto> cotationDtoSet;

    public Integer getEsmsCriteriaId() {
        return esmsCriteriaId;
    }

    public void setEsmsCriteriaId(Integer esmsCriteriaId) {
        this.esmsCriteriaId = esmsCriteriaId;
    }

    public String getEsmsCriteriaName() {
        return esmsCriteriaName;
    }

    public void setEsmsCriteriaName(String esmsCriteriaName) {
        this.esmsCriteriaName = esmsCriteriaName;
    }

    public Set<CotationDto> getCotationDtoSet() {
        return cotationDtoSet;
    }

    public void setCotationDtoSet(Set<CotationDto> cotationDtoSet) {
        this.cotationDtoSet = cotationDtoSet;
    }
}
