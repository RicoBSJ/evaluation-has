package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.List;

public class PersonThemesDto {

    private Integer personThemesId;
    private String personThemeName;
    private List<PersonObjectifsDto> personObjectifsDtoList;

    public Integer getPersonThemesId() {
        return personThemesId;
    }

    public void setPersonThemesId(Integer personThemesId) {
        this.personThemesId = personThemesId;
    }

    public String getPersonThemeName() {
        return personThemeName;
    }

    public void setPersonThemeName(String personThemeName) {
        this.personThemeName = personThemeName;
    }

    public List<PersonObjectifsDto> getPersonObjectifsDtoList() {
        return personObjectifsDtoList;
    }

    public void setPersonObjectifsDtoList(List<PersonObjectifsDto> personObjectifsDtoList) {
        this.personObjectifsDtoList = personObjectifsDtoList;
    }
}
