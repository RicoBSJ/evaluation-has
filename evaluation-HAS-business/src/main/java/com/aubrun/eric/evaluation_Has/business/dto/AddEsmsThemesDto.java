package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.Collections;
import java.util.List;

public class AddEsmsThemesDto {

    List<Integer> esmsThemes;

    public AddEsmsThemesDto(int esmsThemes) {
        this.esmsThemes = Collections.singletonList(esmsThemes);
    }

    public List<Integer> getEsmsThemes() {
        return esmsThemes;
    }

    public void setEsmsThemes(List<Integer> esmsThemes) {
        this.esmsThemes = esmsThemes;
    }
}
