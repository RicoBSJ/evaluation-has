package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.*;
import com.aubrun.eric.evaluation_Has.business.mapper.*;
import com.aubrun.eric.evaluation_Has.consumer.*;
import com.aubrun.eric.evaluation_Has.models.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final ProfessionalsThemesRepository professionalsThemesRepository;
    private final PersonThemesRepository personThemesRepository;
    private final ESMSThemesRepository esmsThemesRepository;
    private final ProfessionalsCriteriaRepository professionalsCriteriaRepository;
    private final CotationRepository cotationRepository;
    private final EstablishmentRepository establishmentRepository;
    private final JuridicProtectionRepository juridicProtectionRepository;
    private final SearchRepository searchRepository;

    public CustomerService(CustomerRepository customerRepository, EvaluationHasUserRepository evaluationHasUserRepository, ProfessionalsThemesRepository professionalsThemesRepository, PersonThemesRepository personThemesRepository, ESMSThemesRepository esmsThemesRepository, ProfessionalsCriteriaRepository professionalsCriteriaRepository, CotationRepository cotationRepository, EstablishmentRepository establishmentRepository, JuridicProtectionRepository juridicProtectionRepository, SearchRepository searchRepository) {
        this.customerRepository = customerRepository;
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.professionalsThemesRepository = professionalsThemesRepository;
        this.personThemesRepository = personThemesRepository;
        this.esmsThemesRepository = esmsThemesRepository;
        this.professionalsCriteriaRepository = professionalsCriteriaRepository;
        this.cotationRepository = cotationRepository;
        this.establishmentRepository = establishmentRepository;
        this.juridicProtectionRepository = juridicProtectionRepository;
        this.searchRepository = searchRepository;
    }


    public List<CustomerDto> findCustomersModerator() {
        return customerRepository.findAll().stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }

    public List<CustomerDto> findAll(Principal principal) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByEmail(principal.getName()).orElseThrow(RuntimeException::new);
        return customerRepository.findAllByEvaluationHASUser(evaluationHASUser).stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }

    public void save(CustomerDto addCustomerDto, int evaluationHasUserId) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        Customer customer = new Customer();
        customer.setCustomerFirstName(addCustomerDto.getCustomerFirstName());
        customer.setCustomerLastName(addCustomerDto.getCustomerLastName());
        customer.setDateBirth(addCustomerDto.getDateBirth());
        customer.setSocialSecurityNumber(addCustomerDto.getSocialSecurityNumber());
        customer.setMutualName(addCustomerDto.getMutualName());
        customer.setEntryDate(addCustomerDto.getEntryDate());
        customer.setAge(addCustomerDto.getAge());
        customer.setEvaluationHASUser(evaluationHASUser);
        Customer customerSave = customerRepository.save(customer);
        evaluationHASUser.getCustomerList().add(customerSave);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void addEvaluationHasUser(int evaluationHasUserId, int customerId) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customer.setEvaluationHASUser(evaluationHASUser);
        evaluationHASUser.getCustomerList().add(customer);
        customerRepository.save(customer);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void addProfessionalsThemes(int customerId, List<Integer> professionalsThemes){
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<ProfessionalsThemes> professionalsThemesList = professionalsThemesRepository.findAllById(professionalsThemes);
        customer.getProfessionalsThemesList().addAll(professionalsThemesList);
        customerRepository.save(customer);
    }

    public void addPersonThemes(int customerId, int personThemesId){
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        PersonThemes personThemes = personThemesRepository.findById(personThemesId).orElseThrow(RuntimeException::new);
        customer.getPersonThemesList().add(personThemes);
        customerRepository.save(customer);
    }

    public void addEsmsThemes(int customerId, List<Integer> esmsThemes){
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<ESMSThemes> esmsThemesList = esmsThemesRepository.findAllById(esmsThemes);
        customer.getEsmsThemesList().addAll(esmsThemesList);
        customerRepository.save(customer);
    }

    public void addEstablishment(int customerId, int establishmentId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        Establishment establishment = establishmentRepository.findById(establishmentId).orElseThrow(RuntimeException::new);
        customer.getEstablishmentList().add(establishment);
        customerRepository.save(customer);
    }

    public void addJuridicProtections(int customerId, int juridicProtectionId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        JuridicProtection juridicProtection = juridicProtectionRepository.findById(juridicProtectionId).orElseThrow(RuntimeException::new);
        customer.getJuridicProtectionSet().add(juridicProtection);
        customerRepository.save(customer);
    }

    public void updateCustomer(int customerId, LocalDateTime releaseDate) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customer.setReleaseDate(releaseDate);
        if (customer.getEvaluationHASUser() != null){
            customer.getEvaluationHASUser().getCustomerList().remove(customer);
        }
        customer.setEvaluationHASUser(null);
        customerRepository.save(customer);
    }

    public void deleteByJuridicProtections(int customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByJuridicProtectionList(customer.getCustomerId());
    }

    public void deleteByEstablishments(int customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByEstablishmentList(customer.getCustomerId());
    }

    public CustomerDto findById(Integer id) {
        return CustomerDtoMapper.toDto(customerRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    public List<CustomerDto> searchCustomer(SearchCustomerDto searchCustomerDto){
        SearchCustomer searchCustomer = SearchCustomerDtoMapper.toEntity(searchCustomerDto);
        return searchRepository.findAllByNameAndDateBirthAndEntryDateAndAge(searchCustomer).stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }
}