package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.annotations.ExcludeClassFromJacocoGeneratedReport;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@ExcludeClassFromJacocoGeneratedReport
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;

    public UserDetailsServiceImpl(EvaluationHasUserRepository evaluationHasUserRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        EvaluationHASUser user = evaluationHasUserRepository.findEvaluationHASUserByEmail(email).orElseThrow(RuntimeException::new);
        return UserDetailsImpl.build(user);
    }
}
