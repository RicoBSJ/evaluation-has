package com.aubrun.eric.evaluation_Has.business.service;

import com.aubrun.eric.evaluation_Has.business.dto.PersonObjectifsDto;
import com.aubrun.eric.evaluation_Has.business.mapper.PersonCriteriaDtoMapper;
import com.aubrun.eric.evaluation_Has.business.mapper.PersonObjectifsDtoMapper;
import com.aubrun.eric.evaluation_Has.consumer.EvaluationHasUserRepository;
import com.aubrun.eric.evaluation_Has.consumer.PersonObjectifsRepository;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import com.aubrun.eric.evaluation_Has.models.PersonObjectifs;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonObjectifsService {

    private final EvaluationHasUserRepository evaluationHasUserRepository;
    private final PersonObjectifsRepository personObjectifsRepository;

    public PersonObjectifsService(EvaluationHasUserRepository evaluationHasUserRepository, PersonObjectifsRepository personObjectifsRepository) {
        this.evaluationHasUserRepository = evaluationHasUserRepository;
        this.personObjectifsRepository = personObjectifsRepository;
    }

    public List<PersonObjectifsDto> findAll(){
        return personObjectifsRepository.findAll().stream().map(PersonObjectifsDtoMapper::toDto).collect(Collectors.toList());
    }

    public PersonObjectifsDto findById(int personObjectifsId){
        return PersonObjectifsDtoMapper.toDto(personObjectifsRepository.findById(personObjectifsId).orElseThrow(RuntimeException::new));
    }

    public void save(String lastName, PersonObjectifsDto personObjectifsDto){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findEvaluationHASUserByLastName(lastName).orElseThrow(RuntimeException::new);
        PersonObjectifs personObjectifs = new PersonObjectifs();
        personObjectifs.setPersonObjectifName(personObjectifsDto.getPersonObjectifName());
        personObjectifs.setPersonCriteriaList(personObjectifsDto.getPersonCriteriaDtoList().stream().map(PersonCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        personObjectifsRepository.save(personObjectifs);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(PersonObjectifsDto personObjectifsDto, int personObjectifsDtoId){
        PersonObjectifs personObjectifs = personObjectifsRepository.findById(personObjectifsDtoId).orElseThrow(RuntimeException::new);
        personObjectifs.setPersonObjectifName(personObjectifsDto.getPersonObjectifName());
        personObjectifs.setPersonCriteriaList(personObjectifsDto.getPersonCriteriaDtoList().stream().map(PersonCriteriaDtoMapper::toEntity).collect(Collectors.toList()));
        personObjectifsRepository.save(personObjectifs);
    }

    public void deleteById(int personObjectifsDtoId){
        personObjectifsRepository.deleteById(personObjectifsDtoId);
    }
}
