package com.aubrun.eric.evaluation_Has.business.dto;

import java.util.List;

public class ProfessionalsThemesDto {

    private Integer professionalsThemesId;
    private String professionalsThemeName;
    private List<ProfessionalsObjectifsDto> professionalsObjectifsDtoList;

    public Integer getProfessionalsThemesId() {
        return professionalsThemesId;
    }

    public void setProfessionalsThemesId(Integer professionalsThemesId) {
        this.professionalsThemesId = professionalsThemesId;
    }

    public String getProfessionalsThemeName() {
        return professionalsThemeName;
    }

    public void setProfessionalsThemeName(String professionalsThemeName) {
        this.professionalsThemeName = professionalsThemeName;
    }

    public List<ProfessionalsObjectifsDto> getProfessionalsObjectifsDtoList() {
        return professionalsObjectifsDtoList;
    }

    public void setProfessionalsObjectifsDtoList(List<ProfessionalsObjectifsDto> professionalsObjectifsDtoList) {
        this.professionalsObjectifsDtoList = professionalsObjectifsDtoList;
    }
}
