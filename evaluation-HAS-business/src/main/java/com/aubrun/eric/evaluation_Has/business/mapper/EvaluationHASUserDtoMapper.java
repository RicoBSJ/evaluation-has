package com.aubrun.eric.evaluation_Has.business.mapper;

import com.aubrun.eric.evaluation_Has.business.dto.EvaluationHASUserDto;
import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;

import java.util.stream.Collectors;

public class EvaluationHASUserDtoMapper {

    static public EvaluationHASUserDto toDto(EvaluationHASUser evaluationHASUser) {

        EvaluationHASUserDto dto = new EvaluationHASUserDto();
        dto.setEvaluationHasUserId(evaluationHASUser.getEvaluationHasUserId());
        dto.setFirstName(evaluationHASUser.getFirstName());
        dto.setLastName(evaluationHASUser.getLastName());
        dto.setPassword(evaluationHASUser.getPassword());
        dto.setEmail(evaluationHASUser.getEmail());
        dto.setPhoneUser(evaluationHASUser.getPhoneUser());
        dto.setDateBirthUser(evaluationHASUser.getDateBirthUser());
        dto.setSocialSecurityNumberUser(evaluationHASUser.getSocialSecurityNumberUser());
        dto.setEntryDateUser(evaluationHASUser.getEntryDateUser());
        dto.setReleaseDateUser(evaluationHASUser.getReleaseDateUser());
        dto.setAgeUser(evaluationHASUser.getAgeUser());
        dto.setCustomerDtoList(evaluationHASUser.getCustomerList().stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList()));
        dto.setRoleDtoSet(evaluationHASUser.getRoles().stream().map(RoleDtoMapper::toDto).collect(Collectors.toSet()));
        dto.setEstablishmentDtoSet(evaluationHASUser.getEstablishments().stream().map(EstablishmentDtoMapper::toDto).collect(Collectors.toSet()));
        return dto;
    }

    static public EvaluationHASUser toEntity(EvaluationHASUserDto evaluationHASUserDto) {

        EvaluationHASUser entity = new EvaluationHASUser();
        entity.setEvaluationHasUserId(evaluationHASUserDto.getEvaluationHasUserId());
        entity.setFirstName(evaluationHASUserDto.getFirstName());
        entity.setLastName(evaluationHASUserDto.getLastName());
        entity.setPassword(evaluationHASUserDto.getPassword());
        entity.setEmail(evaluationHASUserDto.getEmail());
        entity.setPhoneUser(evaluationHASUserDto.getPhoneUser());
        entity.setDateBirthUser(evaluationHASUserDto.getDateBirthUser());
        entity.setSocialSecurityNumberUser(evaluationHASUserDto.getSocialSecurityNumberUser());
        entity.setEntryDateUser(evaluationHASUserDto.getEntryDateUser());
        entity.setReleaseDateUser(evaluationHASUserDto.getReleaseDateUser());
        entity.setAgeUser(evaluationHASUserDto.getAgeUser());
        entity.setRoles(evaluationHASUserDto.getRoleDtoSet().stream().map(RoleDtoMapper::toEntity).collect(Collectors.toSet()));
        entity.setEstablishments(evaluationHASUserDto.getEstablishmentDtoSet().stream().map(EstablishmentDtoMapper::toEntity).collect(Collectors.toSet()));
        return entity;
    }
}
