package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSObjectifsDto;
import com.aubrun.eric.evaluation_Has.business.service.ESMSObjectifsService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/esmsObjectifs")
public class ESMSObjectifsController {

    private final ESMSObjectifsService esmsObjectifsService;

    public ESMSObjectifsController(ESMSObjectifsService esmsObjectifsService) {
        this.esmsObjectifsService = esmsObjectifsService;
    }

    @GetMapping("/")
    public List<ESMSObjectifsDto> getAllEsmsObjectifs(){
        return this.esmsObjectifsService.findAll();
    }

    @GetMapping("/{id}")
    public ESMSObjectifsDto getEsmsObjectifsById(@PathVariable(value = "id") int esmsObjectifsId){
        return this.esmsObjectifsService.findById(esmsObjectifsId);
    }

    @PostMapping("/")
    public void createEsmsObjectif(Principal principal, @RequestBody ESMSObjectifsDto esmsObjectifsDto){
        esmsObjectifsService.save(principal.getName(), esmsObjectifsDto);
    }

    @PutMapping("/{id}")
    public void updateEsmsObjectifs(@RequestBody ESMSObjectifsDto esmsObjectifsDto, @PathVariable(value = "id") int esmsObjectifsId){
        esmsObjectifsService.update(esmsObjectifsDto, esmsObjectifsId);
    }

    @DeleteMapping("/{id}")
    public void deleteEsmsObjectifs(@PathVariable(value = "id") int esmsObjectifsId){
        esmsObjectifsService.deleteById(esmsObjectifsId);
    }
}
