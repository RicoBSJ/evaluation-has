package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsThemesDto;
import com.aubrun.eric.evaluation_Has.business.service.ProfessionalsThemesService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/professionalsThemes")
public class ProfessionalsThemesController {

    private final ProfessionalsThemesService professionalsThemesService;

    public ProfessionalsThemesController(ProfessionalsThemesService professionalsThemesService) {
        this.professionalsThemesService = professionalsThemesService;
    }

    @GetMapping("/")
    public List<ProfessionalsThemesDto> getAllProfessionalsThemes(){
        return this.professionalsThemesService.findAll();
    }

    @GetMapping("/{id}")
    public ProfessionalsThemesDto getProfessionalsThemesById(@PathVariable(value = "id") int professionalsThemesId){
        return this.professionalsThemesService.findById(professionalsThemesId);
    }

    @PostMapping("/")
    public void createProfessionalsThemes(Principal principal, @RequestBody ProfessionalsThemesDto professionalsThemesDto){
        professionalsThemesService.save(principal.getName(), professionalsThemesDto);
    }

    @PutMapping("/{id}")
    public void updateProfessionalsThemes(@RequestBody ProfessionalsThemesDto professionalsThemesDto, @PathVariable(value = "id") int professionalsThemesId){
        professionalsThemesService.update(professionalsThemesDto, professionalsThemesId);
    }

    @DeleteMapping("/{id}")
    public void deleteProfessionalsThemes(@PathVariable(value = "id") int professionalsThemesId){
        professionalsThemesService.deleteById(professionalsThemesId);
    }
}
