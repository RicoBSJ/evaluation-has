package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.PersonObjectifsDto;
import com.aubrun.eric.evaluation_Has.business.service.PersonObjectifsService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/personObjectifs")
public class PersonObjectifsController {

    private final PersonObjectifsService personObjectifsService;

    public PersonObjectifsController(PersonObjectifsService personObjectifsService) {
        this.personObjectifsService = personObjectifsService;
    }

    @GetMapping("/")
    public List<PersonObjectifsDto> getAllPersonObjectifs(){
        return this.personObjectifsService.findAll();
    }

    @GetMapping("/{id}")
    public PersonObjectifsDto getPersonObjectifsById(@PathVariable(value = "id") int personObjectifsId){
        return this.personObjectifsService.findById(personObjectifsId);
    }

    @PostMapping("/")
    public void createPersonObjectifs(Principal principal, @RequestBody PersonObjectifsDto personObjectifsDto){
        personObjectifsService.save(principal.getName(), personObjectifsDto);
    }

    @PutMapping("/{id}")
    public void updatePersonObjectifs(@RequestBody PersonObjectifsDto personObjectifsDto, @PathVariable(value = "id") int personObjectifsId){
        personObjectifsService.update(personObjectifsDto, personObjectifsId);
    }

    @DeleteMapping("/{id}")
    public void deletePersonObjectifs(@PathVariable(value = "id") int personObjectifsId){
        personObjectifsService.deleteById(personObjectifsId);
    }
}
