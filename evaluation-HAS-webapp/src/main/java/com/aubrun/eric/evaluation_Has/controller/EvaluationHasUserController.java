package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.AddEstablishmentsDto;
import com.aubrun.eric.evaluation_Has.business.dto.AddRoleDto;
import com.aubrun.eric.evaluation_Has.business.dto.EvaluationHASUserDto;
import com.aubrun.eric.evaluation_Has.business.service.EvaluationHasUserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class EvaluationHasUserController {

    private final EvaluationHasUserService evaluationHasUserService;

    public EvaluationHasUserController(EvaluationHasUserService evaluationHasUserService) {
        this.evaluationHasUserService = evaluationHasUserService;
    }

    @GetMapping("/")
    public List<EvaluationHASUserDto> getAllEvaluationHasUser(){
        return this.evaluationHasUserService.findAll();
    }

    @GetMapping("/{id}")
    public EvaluationHASUserDto getEvaluationHasUserById(@PathVariable(value = "id") int userId) {
        return this.evaluationHasUserService.findById(userId);
    }

    @PostMapping("/")
    public void createEvaluationHasUser(@RequestBody EvaluationHASUserDto evaluationHASUserDto){
        evaluationHasUserService.save(evaluationHASUserDto);
    }

    @DeleteMapping("/{id}")
    public void deleteEvaluationHasUser(@PathVariable("id") int userId){
        evaluationHasUserService.delete(userId);
    }

    @PutMapping("/addReleaseDateUser/{id}")
    public void updateUser(@RequestBody EvaluationHASUserDto evaluationHASUserDto, @PathVariable(value = "id") int evaluationHasUserId){
        evaluationHasUserService.updateUser(evaluationHasUserId, evaluationHASUserDto.getReleaseDateUser());
    }

    @PutMapping("/addEstablishmentUser/{id}")
    public void addEstablishment(@RequestBody AddEstablishmentsDto addEstablishmentsDto, @PathVariable(value = "id") int evaluationHasUserId){
        evaluationHasUserService.addEstablishment(evaluationHasUserId, addEstablishmentsDto.getEstablishments());
    }

    @PutMapping("/addRole/{id}")
    public void addRole(@RequestBody AddRoleDto addRoleDto, @PathVariable(value = "id") int evaluationHasUserId){
        evaluationHasUserService.addRole(evaluationHasUserId, addRoleDto.getRoles());
    }

    @DeleteMapping("/delEstablishment/{id}")
    public void deleteByEstablishment(@PathVariable(value = "id") int evaluationHasUserId){
        evaluationHasUserService.deleteEstablishment(evaluationHasUserId);
    }

    @DeleteMapping("/delRole/{id}")
    public void deleteRole(@PathVariable(value = "id") int evaluationHasUserId){
        evaluationHasUserService.deleteRole(evaluationHasUserId);
    }

}
