package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSCriteriaDto;
import com.aubrun.eric.evaluation_Has.business.service.ESMSCriteriaService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/esmsCriteria")
public class ESMSCriteriaController {

    private final ESMSCriteriaService esmsCriteriaService;

    public ESMSCriteriaController(ESMSCriteriaService esmsCriteriaService) {
        this.esmsCriteriaService = esmsCriteriaService;
    }

    @GetMapping("/")
    public List<ESMSCriteriaDto> getAllEsmsCriteria(){
        return this.esmsCriteriaService.findAll();
    }

    @GetMapping("/{id}")
    public ESMSCriteriaDto getEsmsCriteriaById(@PathVariable(value = "id") int esmsCriteriaId){
        return this.esmsCriteriaService.findById(esmsCriteriaId);
    }

    @PostMapping("/")
    public void createEsmsCriteria(Principal principal, @RequestBody ESMSCriteriaDto esmsCriteriaDto){
        esmsCriteriaService.save(principal.getName(), esmsCriteriaDto);
    }

    @PutMapping("/{id}")
    public void updateEsmsCriteria(@RequestBody ESMSCriteriaDto esmsCriteriaDto, @PathVariable(value = "id") int esmsCriteriaId){
        esmsCriteriaService.update(esmsCriteriaDto, esmsCriteriaId);
    }

    @DeleteMapping("/{id}")
    public void deleteEsmsCriteria(@PathVariable(value = "id") int esmsCriteriaId){
        esmsCriteriaService.deleteById(esmsCriteriaId);
    }
}
