package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.PersonThemesDto;
import com.aubrun.eric.evaluation_Has.business.service.PersonThemesService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/personThemes")
public class PersonThemesController {

    private final PersonThemesService personThemesService;

    public PersonThemesController(PersonThemesService personThemesService) {
        this.personThemesService = personThemesService;
    }

    @GetMapping("/")
    public List<PersonThemesDto> getAllPersonThemes(){
        return this.personThemesService.findAll();
    }

    @GetMapping("/{id}")
    public PersonThemesDto getPersonThemesById(@PathVariable(value = "id") int personThemesId){
        return this.personThemesService.findById(personThemesId);
    }

    @PostMapping("/")
    public void createPersonThemes(Principal principal, @RequestBody PersonThemesDto personThemesDto){
        personThemesService.save(principal.getName(), personThemesDto);
    }

    @PutMapping("/{id}")
    public void updatePersonThemes(@RequestBody PersonThemesDto personThemesDto, @PathVariable(value = "id") int personThemesId){
        personThemesService.update(personThemesDto, personThemesId);
    }

    @DeleteMapping("/{id}")
    public void deletePersonThemes(@PathVariable(value = "id") int personThemesId){
        personThemesService.deleteById(personThemesId);
    }
}
