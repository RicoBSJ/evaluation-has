package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.PersonCriteriaDto;
import com.aubrun.eric.evaluation_Has.business.service.PersonCriteriaService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/personCriteria")
public class PersonCriteriaController {

    private final PersonCriteriaService personCriteriaService;

    public PersonCriteriaController(PersonCriteriaService personCriteriaService) {
        this.personCriteriaService = personCriteriaService;
    }

    @GetMapping("/")
    public List<PersonCriteriaDto> getAllPersonCriteria(){
        return this.personCriteriaService.findAll();
    }

    @GetMapping("/{id}")
    public PersonCriteriaDto getPersonCriteriaById(@PathVariable(value = "id") int personCriteriaId){
        return this.personCriteriaService.findById(personCriteriaId);
    }

    @PostMapping("/")
    public void createPersonCriteria(Principal principal, @RequestBody PersonCriteriaDto personCriteriaDto){
        personCriteriaService.save(principal.getName(), personCriteriaDto);
    }

    @PutMapping("/{id}")
    public void updatePersonCriteria(@RequestBody PersonCriteriaDto personCriteriaDto, @PathVariable(value = "id") int personCriteriaId){
        personCriteriaService.update(personCriteriaDto, personCriteriaId);
    }

    @DeleteMapping("/{id}")
    public void deletePersonCriteria(@PathVariable(value = "id") int personCriteriaId){
        personCriteriaService.deleteById(personCriteriaId);
    }
}
