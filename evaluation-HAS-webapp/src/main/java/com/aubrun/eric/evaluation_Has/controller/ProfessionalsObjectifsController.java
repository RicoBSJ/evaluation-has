package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsObjectifsDto;
import com.aubrun.eric.evaluation_Has.business.service.ProfessionalsObjectifsService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/professionalsObjectifs")
public class ProfessionalsObjectifsController {

    private final ProfessionalsObjectifsService professionalsObjectifsService;

    public ProfessionalsObjectifsController(ProfessionalsObjectifsService professionalsObjectifsService) {
        this.professionalsObjectifsService = professionalsObjectifsService;
    }

    @GetMapping("/")
    public List<ProfessionalsObjectifsDto> getAllProfessionalsObjectifs(){
        return this.professionalsObjectifsService.findAll();
    }

    @GetMapping("/{id}")
    public ProfessionalsObjectifsDto getProfessionalsObjectifsById(@PathVariable(value = "id") int professionalsObjectifsId){
        return this.professionalsObjectifsService.findById(professionalsObjectifsId);
    }

    @PostMapping("/")
    public void createProfessionalsObjectifs(Principal principal, @RequestBody ProfessionalsObjectifsDto professionalsObjectifsDto){
        professionalsObjectifsService.save(principal.getName(), professionalsObjectifsDto);
    }

    @PutMapping("/{id}")
    public void updateProfessionalsObjectifs(@RequestBody ProfessionalsObjectifsDto professionalsObjectifsDto, @PathVariable(value = "id") int professionalsObjectifId){
        professionalsObjectifsService.update(professionalsObjectifsDto, professionalsObjectifId);
    }

    @DeleteMapping("/{id}")
    public void deleteProfessionalsObjectifs(@PathVariable(value = "id") int professionalsObjectifsId){
        professionalsObjectifsService.deleteById(professionalsObjectifsId);
    }
}
