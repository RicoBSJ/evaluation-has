package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.ProfessionalsCriteriaDto;
import com.aubrun.eric.evaluation_Has.business.service.ProfessionalsCriteriaService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/professionalsCriteria")
public class ProfessionalsCriteriaController {

    private final ProfessionalsCriteriaService professionalsCriteriaService;

    public ProfessionalsCriteriaController(ProfessionalsCriteriaService professionalsCriteriaService) {
        this.professionalsCriteriaService = professionalsCriteriaService;
    }

    @GetMapping("/")
    public List<ProfessionalsCriteriaDto> getAllProfessionalsCriteria(){
        return this.professionalsCriteriaService.findAll();
    }

    @GetMapping("/{id}")
    public ProfessionalsCriteriaDto getProfessionalsCriteriaById(@PathVariable(value = "id") int professionalsCriteriaId){
        return this.professionalsCriteriaService.findById(professionalsCriteriaId);
    }

    @PostMapping("/{id}")
    public void createProfessionalsCriteria(Principal principal, @RequestBody ProfessionalsCriteriaDto professionalsCriteriaDto, @PathVariable(value = "id") int customerId){
        professionalsCriteriaService.save(principal.getName(), professionalsCriteriaDto, customerId);
    }
    /*
    @PostMapping("/")
    public void createCustomer(@RequestBody CustomerDto customerDto) {
        customerService.save(customerDto, Integer.parseInt(customerDto.getEvaluationHASUser()));
    }
     */
    @PutMapping("/{id}")
    public void updateProfessionalsCriteria(@RequestBody ProfessionalsCriteriaDto professionalsCriteriaDto, @PathVariable(value = "id") int customerId){
        professionalsCriteriaService.update(professionalsCriteriaDto, customerId);
    }

    @DeleteMapping("/{id}")
    public void deleteProfessionalsCriteria(@PathVariable(value = "id") int professionalsCriteriaId){
        professionalsCriteriaService.deleteById(professionalsCriteriaId);
    }
}
