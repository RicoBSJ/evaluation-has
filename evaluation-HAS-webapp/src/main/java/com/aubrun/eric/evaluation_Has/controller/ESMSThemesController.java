package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.ESMSThemesDto;
import com.aubrun.eric.evaluation_Has.business.service.ESMSThemesService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/esmsThemes")
public class ESMSThemesController {

    private final ESMSThemesService esmsThemesService;

    public ESMSThemesController(ESMSThemesService esmsThemesService) {
        this.esmsThemesService = esmsThemesService;
    }

    @GetMapping("/")
    public List<ESMSThemesDto> getAllEsmsThemes(){
        return this.esmsThemesService.findAll();
    }

    @GetMapping("/{id}")
    public ESMSThemesDto getEsmsThemesById(@PathVariable(value = "id") int esmsThemesId){
        return this.esmsThemesService.findById(esmsThemesId);
    }

    @PostMapping("/")
    public void createEsmsThemes(Principal principal, @RequestBody ESMSThemesDto esmsThemesDto){
        esmsThemesService.save(principal.getName(), esmsThemesDto);
    }

    @PutMapping("/{id}")
    public void updateEsmsThemes(@RequestBody ESMSThemesDto esmsThemesDto, @PathVariable(value = "id") int esmsThemesId){
        esmsThemesService.update(esmsThemesDto, esmsThemesId);
    }

    @DeleteMapping("/{id}")
    public void deleteEsmsThemes(@PathVariable(value = "id") int esmsThemesId){
        esmsThemesService.deleteById(esmsThemesId);
    }
}
