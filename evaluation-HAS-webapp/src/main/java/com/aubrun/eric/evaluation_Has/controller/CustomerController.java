package com.aubrun.eric.evaluation_Has.controller;

import com.aubrun.eric.evaluation_Has.business.dto.*;
import com.aubrun.eric.evaluation_Has.business.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/moderatorAccess")
    public List<CustomerDto> getCustomersModerator() {
        return this.customerService.findCustomersModerator();
    }

    @GetMapping("/")
    public List<CustomerDto> getAllCustomers(Principal principal) {
        return this.customerService.findAll(principal);
    }

    @GetMapping("/{id}")
    public CustomerDto getCustomerById(@PathVariable(value = "id") int customerId) {
        return this.customerService.findById(customerId);
    }

    @PostMapping("/")
    public void createCustomer(@RequestBody CustomerDto customerDto) {
        customerService.save(customerDto, Integer.parseInt(customerDto.getEvaluationHASUser()));
    }

    @PutMapping("/addReleaseDate/{id}")
    public void updateCustomer(@RequestBody CustomerDto customerDto, @PathVariable(value = "id") int customerId) {
        customerService.updateCustomer(customerId, customerDto.getReleaseDate());
    }

    @PutMapping("/addEvaluationHasUser/{id}")
    public void addEvaluationHasUser(@RequestBody EvaluationHASUserDto evaluationHASUserDto, @PathVariable(value = "id") int customerId) {
        customerService.addEvaluationHasUser(evaluationHASUserDto.getEvaluationHasUserId(), customerId);
    }

    @PutMapping("/addProfessionalsThemes/{id}")
    public void addProfessionalsThemes(@RequestBody AddProfessionalsThemesDto professionalsThemesDto, @PathVariable(value = "id") int customerId){
        customerService.addProfessionalsThemes(customerId, professionalsThemesDto.getProfessionalsThemes());
    }

    @PutMapping("/addPersonThemes/{id}")
    public void addPersonThemes(@RequestBody PersonThemesDto personThemesDto, @PathVariable(value = "id") int customerId){
        customerService.addPersonThemes(customerId, personThemesDto.getPersonThemesId());
    }

    @PutMapping("/addEsmsThemes/{id}")
    public void addEsmsThemes(@RequestBody AddEsmsThemesDto esmsThemesDto, @PathVariable(value = "id") int customerId){
        customerService.addEsmsThemes(customerId, esmsThemesDto.getEsmsThemes());
    }

    @PutMapping("/addEstablishmentCustomer/{id}")
    public void addEstablishment(@RequestBody EstablishmentDto establishmentDto, @PathVariable(value = "id") int customerId) {
        customerService.addEstablishment(customerId, establishmentDto.getEstablishmentId());
    }

    @PutMapping("/addJuridicProtections/{id}")
    public void addJuridicProtections(@RequestBody JuridicProtectionDto juridicProtectionDto, @PathVariable(value = "id") int customerId) {
        customerService.addJuridicProtections(customerId, juridicProtectionDto.getJuridicProtectionId());
    }

    @PostMapping(value = "/search")
    private List<CustomerDto> search(@RequestBody SearchCustomerDto searchCustomerDto){
        return customerService.searchCustomer(searchCustomerDto);
    }

    @DeleteMapping("/delJuridicProtection/{id}")
    public void deleteByJuridicProtections(@PathVariable(value = "id") int customerId) {
        customerService.deleteByJuridicProtections(customerId);
    }

    @DeleteMapping("/delEstablishment/{id}")
    public void deleteByEstablishments(@PathVariable(value = "id") int customerId) {
        customerService.deleteByEstablishments(customerId);
    }
}