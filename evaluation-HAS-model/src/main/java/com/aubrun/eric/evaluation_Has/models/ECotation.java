package com.aubrun.eric.evaluation_Has.models;

public enum ECotation {

    PAS_DU_TOUT_SATISFAISANT,
    PLUTOT_PAS_SATISFAISANT,
    PLUTOT_SATISFAISANT,
    TOUT_A_FAIT_SATISFAISANT,
    OPTIMISE,
    NON_CONCERNE,
    REPONSE_INADAPTEE
}
