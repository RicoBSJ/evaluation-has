package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@SequenceGenerator(name = "evaluation_has_user_id_generator", sequenceName = "evaluation_has_user_id_seq", allocationSize = 1)
@Table(name = "EVALUATION_HAS_USER")
public class EvaluationHASUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "evaluation_has_user_id_generator")
    @Column(name = "evaluation_has_user_id")
    private Integer evaluationHasUserId;
    @Column(name = "evaluation_has_first_name")
    private String firstName;
    @Column(name = "evaluation_has_last_name")
    private String lastName;
    @Column(name = "evaluation_has_user_password")
    private String password;
    @Column(name = "evaluation_has_user_email")
    private String email;
    @Column(name = "phone_user")
    private Integer phoneUser;
    @Column(name = "date_birth_user")
    private LocalDate dateBirthUser;
    @Column(name = "social_security_number_user")
    private Long socialSecurityNumberUser;
    @Column(name = "entry_date_user")
    private LocalDateTime entryDateUser;
    @Column(name = "release_date_user")
    private LocalDateTime releaseDateUser;
    @Column(name = "age_user")
    private Integer ageUser;
    @OneToMany
    @JoinColumn(name = "customer_list")
    private List<Customer> customerList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_roles_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_establishments",
            joinColumns = @JoinColumn(name = "user_establishment_id"),
            inverseJoinColumns = @JoinColumn(name = "establishment_id"))
    private Set<Establishment> establishments;

    public EvaluationHASUser() {
    }

    public EvaluationHASUser(Integer ageUser, Integer phoneUser, Long socialSecurityNumberUser, LocalDate dateBirthUser,
                             LocalDateTime entryDateUser,
                             String firstName, String lastName,
                             String username, String password) {
        this.ageUser = ageUser;
        this.phoneUser = phoneUser;
        this.socialSecurityNumberUser = socialSecurityNumberUser;
        this.dateBirthUser = dateBirthUser;
        this.entryDateUser = entryDateUser;
        this.lastName = lastName;
        this.firstName = firstName;
        this.password = password;
        this.email = username;
    }

    public Integer getEvaluationHasUserId() {
        return evaluationHasUserId;
    }

    public void setEvaluationHasUserId(Integer evaluationHasUserId) {
        this.evaluationHasUserId = evaluationHasUserId;
    }

    public String getUsername() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPhoneUser() {
        return phoneUser;
    }

    public void setPhoneUser(Integer phoneUser) {
        this.phoneUser = phoneUser;
    }

    public LocalDate getDateBirthUser() {
        return dateBirthUser;
    }

    public void setDateBirthUser(LocalDate dateBirthUser) {
        this.dateBirthUser = dateBirthUser;
    }

    public Long getSocialSecurityNumberUser() {
        return socialSecurityNumberUser;
    }

    public void setSocialSecurityNumberUser(Long socialSecurityNumberUser) {
        this.socialSecurityNumberUser = socialSecurityNumberUser;
    }

    public LocalDateTime getEntryDateUser() {
        return entryDateUser;
    }

    public void setEntryDateUser(LocalDateTime entryDateUser) {
        this.entryDateUser = entryDateUser;
    }

    public LocalDateTime getReleaseDateUser() {
        return releaseDateUser;
    }

    public void setReleaseDateUser(LocalDateTime releaseDateUser) {
        this.releaseDateUser = releaseDateUser;
    }

    public Integer getAgeUser() {
        return ageUser;
    }

    public void setAgeUser(Integer ageUser) {
        this.ageUser = ageUser;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Establishment> getEstablishments() {
        return establishments;
    }

    public void setEstablishments(Set<Establishment> establishments) {
        this.establishments = establishments;
    }

    @Override
    public String toString() {
        return "EvaluationHASUser{" +
                "evaluationHasUserId=" + evaluationHasUserId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phoneUser=" + phoneUser +
                ", dateBirthUser=" + dateBirthUser +
                ", socialSecurityNumberUser=" + socialSecurityNumberUser +
                ", entryDateUser=" + entryDateUser +
                ", releaseDateUser=" + releaseDateUser +
                ", ageUser=" + ageUser +
                ", customerList=" + customerList +
                ", roles=" + roles +
                ", establishments=" + establishments +
                '}';
    }
}
