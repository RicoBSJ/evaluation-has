package com.aubrun.eric.evaluation_Has.models;

public enum ERole {

    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
