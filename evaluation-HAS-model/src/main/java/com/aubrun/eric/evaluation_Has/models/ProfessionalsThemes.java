package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;

import java.util.List;

@Entity
@SequenceGenerator(name = "professionals_themes_id_generator", sequenceName = "professionals_themes_id_seq", allocationSize = 1)
@Table(name = "PROFESSIONALS_THEMES")
public class ProfessionalsThemes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "professionals_themes_id_generator")
    @Column(name = "professionals_themes_id")
    private Integer professionalsThemesId;
    @Column(name = "professionals_theme_name")
    private String professionalsThemeName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "professionals_objectifs_list",
            joinColumns = @JoinColumn(name = "professionals_themes_id"),
            inverseJoinColumns = @JoinColumn(name = "professionals_objectifs_id"))
    private List<ProfessionalsObjectifs> professionalsObjectifsList;

    public ProfessionalsThemes() {
    }

    public Integer getProfessionalsThemesId() {
        return professionalsThemesId;
    }

    public void setProfessionalsThemesId(Integer professionalsThemesId) {
        this.professionalsThemesId = professionalsThemesId;
    }

    public String getProfessionalsThemeName() {
        return professionalsThemeName;
    }

    public void setProfessionalsThemeName(String professionalsThemeName) {
        this.professionalsThemeName = professionalsThemeName;
    }

    public List<ProfessionalsObjectifs> getProfessionalsObjectifsList() {
        return professionalsObjectifsList;
    }

    public void setProfessionalsObjectifsList(List<ProfessionalsObjectifs> professionalsObjectifsList) {
        this.professionalsObjectifsList = professionalsObjectifsList;
    }

    @Override
    public String toString() {
        return "ProfessionalsThemes{" +
                "professionalsThemesId=" + professionalsThemesId +
                ", professionalsThemeName='" + professionalsThemeName + '\'' +
                ", professionalsObjectifsList=" + professionalsObjectifsList +
                '}';
    }
}
