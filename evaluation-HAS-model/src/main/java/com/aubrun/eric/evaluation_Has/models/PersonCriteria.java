package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@SequenceGenerator(name = "person_criteria_id_generator", sequenceName = "person_criteria_id_seq", allocationSize = 1)
@Table(name = "PERSON_CRITERIA")
public class PersonCriteria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "person_criteria_id_generator")
    @Column(name = "person_criteria_id")
    private Integer personCriteriaId;
    @Column(name = "person_criteria_name")
    private String personCriteriaName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "person_cotation_list",
            joinColumns = @JoinColumn(name = "person_criteria_id"),
            inverseJoinColumns = @JoinColumn(name = "ecotation_id"))
    private Set<Cotation> cotationSet;

    public PersonCriteria() {
    }

    public Integer getPersonCriteriaId() {
        return personCriteriaId;
    }

    public void setPersonCriteriaId(Integer personCriteriaId) {
        this.personCriteriaId = personCriteriaId;
    }

    public String getPersonCriteriaName() {
        return personCriteriaName;
    }

    public void setPersonCriteriaName(String personCriteriaName) {
        this.personCriteriaName = personCriteriaName;
    }

    public Set<Cotation> getCotationSet() {
        return cotationSet;
    }

    public void setCotationSet(Set<Cotation> cotationSet) {
        this.cotationSet = cotationSet;
    }

    @Override
    public String toString() {
        return "PersonCriteria{" +
                "personCriteriaId=" + personCriteriaId +
                ", personCriteriaName='" + personCriteriaName + '\'' +
                ", cotationSet=" + cotationSet +
                '}';
    }
}
