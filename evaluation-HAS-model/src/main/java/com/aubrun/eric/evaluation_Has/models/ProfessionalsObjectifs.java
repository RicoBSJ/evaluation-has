package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;

import java.util.List;

@Entity
@SequenceGenerator(name = "professionals_objectifs_id_generator", sequenceName = "professionals_objectifs_id_seq", allocationSize = 1)
@Table(name = "PROFESSIONALS_OBJECTIFS")
public class ProfessionalsObjectifs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "professionals_objectifs_id_generator")
    @Column(name = "professionals_objectifs_id")
    private Integer professionalsObjectifsId;
    @Column(name = "professionals_objectifs_name")
    private String professionalsObjectifsName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "professionals_criteria_list",
            joinColumns = @JoinColumn(name = "professionals_objectifs_id"),
            inverseJoinColumns = @JoinColumn(name = "professionals_criteria_id"))
    private List<ProfessionalsCriteria> professionalsCriteriaList;

    public ProfessionalsObjectifs() {
    }

    public Integer getProfessionalsObjectifsId() {
        return professionalsObjectifsId;
    }

    public void setProfessionalsObjectifsId(Integer professionalsObjectifsId) {
        this.professionalsObjectifsId = professionalsObjectifsId;
    }

    public String getProfessionalsObjectifsName() {
        return professionalsObjectifsName;
    }

    public void setProfessionalsObjectifsName(String professionalsObjectifsName) {
        this.professionalsObjectifsName = professionalsObjectifsName;
    }

    public List<ProfessionalsCriteria> getProfessionalsCriteriaList() {
        return professionalsCriteriaList;
    }

    public void setProfessionalsCriteriaList(List<ProfessionalsCriteria> professionalsCriteriaList) {
        this.professionalsCriteriaList = professionalsCriteriaList;
    }

    @Override
    public String toString() {
        return "ProfessionalsObjectifs{" +
                "professionalsObjectifsId=" + professionalsObjectifsId +
                ", professionalsObjectifsName='" + professionalsObjectifsName + '\'' +
                ", professionalsCriteriaList=" + professionalsCriteriaList +
                '}';
    }
}
