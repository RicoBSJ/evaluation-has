package com.aubrun.eric.evaluation_Has.models;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "cotation_id_generator", sequenceName = "cotation_id_seq", allocationSize = 1)
@Table(name = "COTATION")
@Proxy(lazy = false)
public class Cotation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "cotation_id_generator")
    @Column(name = "cotation_id")
    private Integer cotationId;
    @Enumerated(EnumType.STRING)
    @Column(name = "cotation_name")
    private ECotation eCotation;

    public Cotation() {
    }

    public Integer getCotationId() {
        return cotationId;
    }

    public void setCotationId(Integer cotationId) {
        this.cotationId = cotationId;
    }

    public ECotation geteCotation() {
        return eCotation;
    }

    public void seteCotation(ECotation eCotation) {
        this.eCotation = eCotation;
    }

    @Override
    public String toString() {
        return "Cotation{" +
                "cotationId=" + cotationId +
                ", eCotation=" + eCotation +
                '}';
    }
}