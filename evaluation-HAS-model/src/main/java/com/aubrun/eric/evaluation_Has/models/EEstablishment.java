package com.aubrun.eric.evaluation_Has.models;

public enum EEstablishment {

    FOYER_HEB,
    FOYER_VIE,
    OTHER
}
