package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(name = "professionals_criteria_id_generator", sequenceName = "professionals_criteria_id_seq", allocationSize = 1)
@Table(name = "PROFESSIONALS_CRITERIA")
public class ProfessionalsCriteria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "professionals_criteria_id_generator")
    @Column(name = "professionals_criteria_id")
    private Integer professionalsCriteriaId;
    @Column(name = "professionals_criteria_name")
    private String professionalsCriteriaName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "professionals_cotation_list",
            joinColumns = @JoinColumn(name = "professionals_criteria_id"),
            inverseJoinColumns = @JoinColumn(name = "ecotation_id"))
    private List<Cotation> cotationList;

    public ProfessionalsCriteria() {
    }

    public Integer getProfessionalsCriteriaId() {
        return professionalsCriteriaId;
    }

    public void setProfessionalsCriteriaId(Integer professionalsCriteriaId) {
        this.professionalsCriteriaId = professionalsCriteriaId;
    }

    public String getProfessionalsCriteriaName() {
        return professionalsCriteriaName;
    }

    public void setProfessionalsCriteriaName(String professionalsCriteriaName) {
        this.professionalsCriteriaName = professionalsCriteriaName;
    }

    public List<Cotation> getCotationList() {
        return cotationList;
    }

    public void setCotationList(List<Cotation> cotationList) {
        this.cotationList = cotationList;
    }

    @Override
    public String toString() {
        return "ProfessionalsCriteria{" +
                "professionalsCriteriaId=" + professionalsCriteriaId +
                ", professionalsCriteriaName='" + professionalsCriteriaName + '\'' +
                ", cotationList=" + cotationList +
                '}';
    }
}
