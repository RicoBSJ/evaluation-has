package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@SequenceGenerator(name = "esms_criteria_id_generator", sequenceName = "esms_criteria_id_seq", allocationSize = 1)
@Table(name = "ESMS_CRITERIA")
public class ESMSCriteria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "esms_criteria_id_generator")
    @Column(name = "esms_criteria_id")
    private Integer esmsCriteriaId;
    @Column(name = "esms_criteria_name")
    private String esmsCriteriaName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "esms_cotation_list",
            joinColumns = @JoinColumn(name = "esms_criteria_id"),
            inverseJoinColumns = @JoinColumn(name = "ecotation_id"))
    private Set<Cotation> cotationSet;

    public ESMSCriteria() {
    }

    public Integer getEsmsCriteriaId() {
        return esmsCriteriaId;
    }

    public void setEsmsCriteriaId(Integer esmsCriteriaId) {
        this.esmsCriteriaId = esmsCriteriaId;
    }

    public String getEsmsCriteriaName() {
        return esmsCriteriaName;
    }

    public void setEsmsCriteriaName(String esmsCriteriaName) {
        this.esmsCriteriaName = esmsCriteriaName;
    }

    public Set<Cotation> getCotationSet() {
        return cotationSet;
    }

    public void setCotationSet(Set<Cotation> cotationSet) {
        this.cotationSet = cotationSet;
    }

    @Override
    public String toString() {
        return "ESMSCriteria{" +
                "esmsCriteriaId=" + esmsCriteriaId +
                ", esmsCriteriaName='" + esmsCriteriaName + '\'' +
                ", cotationSet=" + cotationSet +
                '}';
    }
}
