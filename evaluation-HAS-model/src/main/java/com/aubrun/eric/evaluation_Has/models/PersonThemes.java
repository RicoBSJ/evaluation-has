package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;

import java.util.List;

@Entity
@SequenceGenerator(name = "person_themes_id_generator", sequenceName = "person_themes_id_seq", allocationSize = 1)
@Table(name = "PERSON_THEMES")
public class PersonThemes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "person_themes_id_generator")
    @Column(name = "person_themes_id")
    private Integer personThemesId;
    @Column(name = "person_theme_name")
    private String personThemeName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "person_objectifs_list",
            joinColumns = @JoinColumn(name = "person_themes_id"),
            inverseJoinColumns = @JoinColumn(name = "person_objectifs_id"))
    private List<PersonObjectifs> personObjectifsList;

    public PersonThemes() {
    }

    public Integer getPersonThemesId() {
        return personThemesId;
    }

    public void setPersonThemesId(Integer personThemesId) {
        this.personThemesId = personThemesId;
    }

    public String getPersonThemeName() {
        return personThemeName;
    }

    public void setPersonThemeName(String personThemeName) {
        this.personThemeName = personThemeName;
    }

    public List<PersonObjectifs> getPersonObjectifsList() {
        return personObjectifsList;
    }

    public void setPersonObjectifsList(List<PersonObjectifs> personObjectifsList) {
        this.personObjectifsList = personObjectifsList;
    }

    @Override
    public String toString() {
        return "PersonThemes{" +
                "personThemesId=" + personThemesId +
                ", personThemeName='" + personThemeName + '\'' +
                ", personObjectifsList=" + personObjectifsList +
                '}';
    }
}
