package com.aubrun.eric.evaluation_Has.models;

public enum EJuridicProtection {

    TRUSTEESHIP,
    TUTORSHIP,
    FAMILY_GUARDIANSHIP,
    NOT_CONCERNED
}
