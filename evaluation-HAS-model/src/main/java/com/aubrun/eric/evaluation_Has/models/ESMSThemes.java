package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;

import java.util.List;

@Entity
@SequenceGenerator(name = "esms_themes_id_generator", sequenceName = "esms_themes_id_seq", allocationSize = 1)
@Table(name = "ESMS_THEMES")
public class ESMSThemes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "esms_themes_id_generator")
    @Column(name = "esms_themes_id")
    private Integer esmsThemesId;
    @Column(name = "esms_theme_name")
    private String esmsThemeName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "esms_objectifs_list",
            joinColumns = @JoinColumn(name = "esms_themes_id"),
            inverseJoinColumns = @JoinColumn(name = "esms_objectifs_id"))
    private List<ESMSObjectifs> ESMSObjectifsList;

    public ESMSThemes() {
    }

    public Integer getEsmsThemesId() {
        return esmsThemesId;
    }

    public void setEsmsThemesId(Integer esmsThemesId) {
        this.esmsThemesId = esmsThemesId;
    }

    public String getEsmsThemeName() {
        return esmsThemeName;
    }

    public void setEsmsThemeName(String esmsThemeName) {
        this.esmsThemeName = esmsThemeName;
    }

    public List<ESMSObjectifs> getEsmsObjectifsList() {
        return ESMSObjectifsList;
    }

    public void setEsmsObjectifsList(List<ESMSObjectifs> ESMSObjectifsList) {
        this.ESMSObjectifsList = ESMSObjectifsList;
    }

    @Override
    public String toString() {
        return "ESMSThemes{" +
                "esmsThemesId=" + esmsThemesId +
                ", esmsThemeName='" + esmsThemeName + '\'' +
                ", ESMSObjectifsList=" + ESMSObjectifsList +
                '}';
    }
}
