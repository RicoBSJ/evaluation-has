package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;

import java.util.List;

@Entity
@SequenceGenerator(name = "person_objectifs_id_generator", sequenceName = "person_objectifs_id_seq", allocationSize = 1)
@Table(name = "PERSON_OBJECTIFS")
public class PersonObjectifs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "person_objectifs_id_generator")
    @Column(name = "person_objectifs_id")
    private Integer personObjectifsId;
    @Column(name = "person_objectif_name")
    private String personObjectifName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "person_criteria_list",
            joinColumns = @JoinColumn(name = "person_objectifs_id"),
            inverseJoinColumns = @JoinColumn(name = "person_criteria_id"))
    private List<PersonCriteria> personCriteriaList;

    public PersonObjectifs() {
    }

    public Integer getPersonObjectifsId() {
        return personObjectifsId;
    }

    public void setPersonObjectifsId(Integer personObjectifsId) {
        this.personObjectifsId = personObjectifsId;
    }

    public String getPersonObjectifName() {
        return personObjectifName;
    }

    public void setPersonObjectifName(String personObjectifName) {
        this.personObjectifName = personObjectifName;
    }

    public List<PersonCriteria> getPersonCriteriaList() {
        return personCriteriaList;
    }

    public void setPersonCriteriaList(List<PersonCriteria> personCriteriaList) {
        this.personCriteriaList = personCriteriaList;
    }

    @Override
    public String toString() {
        return "PersonObjectifs{" +
                "personObjectifsId=" + personObjectifsId +
                ", personObjectifName='" + personObjectifName + '\'' +
                ", personCriteriaList=" + personCriteriaList +
                '}';
    }
}
