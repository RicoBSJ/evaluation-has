package com.aubrun.eric.evaluation_Has.models;

import javax.persistence.*;

import java.util.List;

@Entity
@SequenceGenerator(name = "esms_objectifs_id_generator", sequenceName = "esms_objectifs_id_seq", allocationSize = 1)
@Table(name = "ESMS_OBJECTIFS")
public class ESMSObjectifs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "esms_objectifs_id_generator")
    @Column(name = "esms_objectifs_id")
    private Integer esmsObjectifsId;
    @Column(name = "esms_objectif_name")
    private String esmsObjectifName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "esms_criteria_list",
            joinColumns = @JoinColumn(name = "esms_objectifs_id"),
            inverseJoinColumns = @JoinColumn(name = "esms_criteria_id"))
    private List<ESMSCriteria> esmsCriteriaList;

    public ESMSObjectifs() {
    }

    public Integer getEsmsObjectifsId() {
        return esmsObjectifsId;
    }

    public void setEsmsObjectifsId(Integer esmsObjectifsId) {
        this.esmsObjectifsId = esmsObjectifsId;
    }

    public String getEsmsObjectifName() {
        return esmsObjectifName;
    }

    public void setEsmsObjectifName(String esmsObjectifName) {
        this.esmsObjectifName = esmsObjectifName;
    }

    public List<ESMSCriteria> getEsmsCriteriaList() {
        return esmsCriteriaList;
    }

    public void setEsmsCriteriaList(List<ESMSCriteria> esmsCriteriaList) {
        this.esmsCriteriaList = esmsCriteriaList;
    }

    @Override
    public String toString() {
        return "ESMSObjectifs{" +
                "esmsObjectifsId=" + esmsObjectifsId +
                ", esmsObjectifName='" + esmsObjectifName + '\'' +
                ", esmsCriteriaList=" + esmsCriteriaList +
                '}';
    }
}
