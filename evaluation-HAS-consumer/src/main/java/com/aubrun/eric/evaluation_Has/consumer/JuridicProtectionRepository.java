package com.aubrun.eric.evaluation_Has.consumer;

import com.aubrun.eric.evaluation_Has.models.Cotation;
import com.aubrun.eric.evaluation_Has.models.JuridicProtection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JuridicProtectionRepository extends JpaRepository<JuridicProtection, Integer> {

}
