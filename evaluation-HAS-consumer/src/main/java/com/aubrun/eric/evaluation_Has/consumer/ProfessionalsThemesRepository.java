package com.aubrun.eric.evaluation_Has.consumer;

import com.aubrun.eric.evaluation_Has.models.ProfessionalsThemes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessionalsThemesRepository extends JpaRepository<ProfessionalsThemes, Integer> {
}
