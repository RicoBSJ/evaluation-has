package com.aubrun.eric.evaluation_Has.consumer;

import com.aubrun.eric.evaluation_Has.models.EvaluationHASUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EvaluationHasUserRepository extends JpaRepository<EvaluationHASUser, Integer> {

    Optional<EvaluationHASUser> findEvaluationHASUserByEmail(String email);
    Optional<EvaluationHASUser> findEvaluationHASUserByLastName(String lastName);
    boolean existsByEmail (String email);

    @Modifying
    @Query(value = "DELETE FROM user_establishments WHERE user_establishment_id=:evaluationHasUserId",nativeQuery = true)
    void delEstablishment(Integer evaluationHasUserId);

    @Modifying
    @Query(value = "DELETE FROM user_roles WHERE user_roles_id=:evaluationHasUserId",nativeQuery = true)
    void delRole(Integer evaluationHasUserId);
}
