package com.aubrun.eric.evaluation_Has.consumer;

import com.aubrun.eric.evaluation_Has.models.Cotation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CotationRepository extends JpaRepository<Cotation, Integer> {
}
