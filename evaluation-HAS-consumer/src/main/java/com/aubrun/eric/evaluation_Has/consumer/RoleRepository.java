package com.aubrun.eric.evaluation_Has.consumer;

import com.aubrun.eric.evaluation_Has.models.ERole;
import com.aubrun.eric.evaluation_Has.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByRoleName(ERole roleName);
}
