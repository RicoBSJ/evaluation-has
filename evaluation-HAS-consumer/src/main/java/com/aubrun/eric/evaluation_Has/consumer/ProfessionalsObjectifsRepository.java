package com.aubrun.eric.evaluation_Has.consumer;

import com.aubrun.eric.evaluation_Has.models.ProfessionalsObjectifs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessionalsObjectifsRepository extends JpaRepository<ProfessionalsObjectifs, Integer> {
}
